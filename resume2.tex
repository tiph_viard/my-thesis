% À merger avec resume.tex lundi matin ...

\section{Introduction}

Contexte ... 

\section{Un langage pour les flots de liens}

	Le premier objectif de cette thèse est de définir un formalisme pour décrire les flux d'interactions.
	 
	Un flot de liens est un triplet $L=(T,V,E)$, $V$ est un ensemble de n\oe{}uds, $T=[\alpha, \omega]$ est un intervalle de temps représentant la durée de vie du flot, et $E\subseteq T\times T\times V\times V$ est l'ensemble des liens (temporels). Un exemple de flot est visible en figure~\ref{fig:flot-exemple}. 

	Un lien $(b,e,u,v)$ dans $E$ signifie que les n\oe{}uds $u$ et $v$ ont interagi du temps $b$ au temps $e$. Nous considérons dans notre travail que les liens sont non-dirigés et qu'il n'y a pas de boucles~\sidenote{Nous ne faisons aucune différence entre $(b,e,u,v)$ et $(b,e,v,u)$ et nous n'autorisons pas les liens $(b,e,u,u)$.}. Un flot est dit \emph{simple} si pour tout $l = (b,e,u,v$, $u\ne v$ et $e>b$, et si pour tout $l = (b,e,u,v)$ et pour tout $l'=(b',e',u,v)$ nous avons $[b,e]\cap[b',e'] = \emptyset$. Nous disons que \emph{$u$ et $v$ interagissent au temps $t$} si il existe un lien $(b,e,u,v)$ dans $E$ tel que $t\in [b,e]$. Par ailleurs nous notons $\overline{l} = e-b$ la durée du lien $l$.

	\begin{figure}
		\includegraphics[width=1\linewidth]{stream-example}
		\label{fig:flot-exemple}
		\caption{titre.}
	\end{figure}

	Étant donné un deux flots de liens $L = (T,V,E)$ et $L'=(T',V',E')$, nous disons que $L'$ est un \emph{sous-flot} de $L$ si $T'\subseteq T$, $V'\subseteq V$, et pour tout $u,v$ dans $V'\times V'$ et $t$ dans $T'$, si $u$ et $v$ interagissent au temps $t$ dans $L'$ alors ils interagissent aussi dans $L$ au temps $t$. Nous définissons le sous-flot induit par un ensemble de paires de n\oe{}uds $S\subseteq V\times V$, $L(S) = (T, V(S), E(S))$, avec $V(S) = \{ v : \exists (v,u) \in S \}$ et $E(S) = \{ (b,e,u,v) : \exists (u,v)\in S \}$. Enfin, nous définissons le sous flot induit par un intervalle de temps $T' = [t,t']\subseteq T$, $L_T' = L_{t..t'} = (T', V, E_{T'})$, avec $E_{T'} = \{ (b',e', u, v) : \exists (b,e,u,v) \in E, [b,e]\cap T' = [b',e'], b'>e' \}$.

	Nous définissons également le graphe induit par un flot de liens, et étudions les relations entre flots de liens et les approches classiques présentées en introduction pour étudier des séquences d'interaction.

	\subsection{Notions basées sur la densité}

	Une notion fondamentale de la théorie des graphes est celle de \emph{densité}. Il s'agit de la probabilité, lorsque l'on choisit deux n\oe{}uds $u$ et $v$ au hasard, qu'il existe un lien entre $u$ et $v$. Intuitivement, il s'agit de mesurer la proportion de liens existant dans le graphe par rapport à tous ceux qui pourraient exister étant donné un nombre de n\oe{}uds~\sidenote{Formellement, pour un graphe $G=(V,E)$, $\delta(G) = \frac{2\cdit |E|}{|V|\cdot (|V|-1)}$}.

	Dans un flot de liens, la densité correspond à la probabilité, lorsque l'on choisit deux n\oe{}uds $u$ et $v$ et un temps $t$ au hasard, que $u$ et $v$ interagissent au temps $t$ :

	\begin{equation}
		\delta(L) = \frac{2\cdot\sum_{l\in E}\overline{l}}{|V|\cdot (|V|-1) \cdot (\omega - \alpha)}
	\end{equation}

	Tout comme dans les graphes on appelle \emph{clique} un sous-graphe de densité maximale, on appelle \emph{clique} un ensemble $(X,[b,e])$, tel que le sous-flot $L_{b..e}(X\times X)$ a pour densité $1$, c'est-à-dire un sous-flot où toutes les paires de n\oe{}uds sont en contact tout le temps. Énumérer toutes les cliques d'un flot de liens pose des questions d'ordre algorithmique, sur lesquelles nous reviendrons plus loin dans ce résumé. 

	Dans de nombreux cas d'application, les liens n'ont pas de durée intrinsèque (c'est-à-dire que l'on a une séquence de $(t,u,v)$ plutôt que de $(b,e,u,v)$). Dans le cas de ces liens, il est adapté de recourir à un temps $\Delta$, puis de transformer la séquence de $(t,u,v)$ en une séquence $(t-\frac{\Delta}{2}, t + \frac{\Delta}{2},u,v)$. On peut ainsi utiliser les notions définies précédemment.

	Nous étendons également les notions de voisinage et de degré aux flots de liens. Le voisinage d'un n\oe{}ud $v\in V$ au temps $t\in T$ est l'ensemble des n\oe{}uds interagissant avec $v$ au temps $t$. Le degré de $v$ au temps $t$ est la taille de ce voisinage. Dans le cas d'un intervalle de temps $[t,t']$, nous ne regardons plus la taille du voisinage. À la place, la contribution de chaque n\oe{}ud au degré de $v$ est ajustée en fonction de la durée de ses interactions avec $v$ :
	\begin{equation}
		d_{t..t'}(v) = \sum_{l\in L_{t..t'}} \frac{\overline{l}}{t'-t}
	\end{equation}
	Et, par extension, le degré moyen du flot est $d(L) = \frac{\sum_{(b,e,u,v)\in E}\frac{e-b}{\omega-\alpha}}{|V|}$. Les notions de densité et de degré conservent la même relation que dans les graphes : $\delta(L) = \frac{d(L)}{|V|-1}$.

	Une notion d'importance dans les développements récents sur les réseaux complexes est celle de \emph{cluster}. Dans un graphe, un cluster est simplement un sous ensemble de n\oe{}uds ou de liens. Dans la pratique, la communauté cherche des clusters au propriétés particulières, par exemple denses, ou ayant une structure communautaire~\sidenote{Le lecteur intéressé peut se référer à~\cite{} ou~\cite{} pour plus d'information.}.

	Un cluster de n\oe{}uds $C$ de $L$ est un sous ensemble de $V\times T$, et $(v,t)\in C$ signifie que $v$ est dans $C$ au temps $t$. L'ensemble de n\oe{}uds induit par $C$ est $V(C) = \{ v : \exists (v,t) \in C \}$. On note $T(C)$ l'intervalle durant lequel des n\oe{}uds sont impliqués dans $C$; $T(C) = [min(t : \exists (v,t)\in C), max(t : \exists (v,t)\in C)]$. Pour tout $v$, nous notons $C(v) = \{t : \exists (v,t)\in C\}$ l'ensemble des temps où $v$ est impliqué dans $C$.

	Nous définissons les flots induits par un cluster de n\oe{}uds, une partition de $L$ en clusters de n\oe{}uds, ainsi que les notions de densité et de degré interne et externe d'un cluster de n\oe{}uds. Un travail similaire est effectué pour les clusters de liens, c'est-à-dire des sous ensembles de $T\times V\times V$.


	\subsection{Notions basées sur les chemins}

	Le temps a un impact important sur l'accessibilité entre les n\oe{}uds. En effet, pour qu'un n\oe{}ud $u$ puisse passer un message à un n\oe{}ud $v$, il faut qu'il existe une séquence de liens temporels formant un chemin entre $u$ et $v$, chaque lien du chemin étant après le précédent. Formellement, un chemin de $u$ à $v$ est une suite de liens $l_1 = (b_1, e_1, u_1, v_1), l_2 = (b_2, e_2, u_2, v_2), \dots, l_k = (b_k, e_k, u_k, v_k)$ tel que $u_0 = u$, $v_k = v$, et pour tout $i=1..k$, $l_i$ est un sous lien d'un lien dans $E$,, $u_i = v_{i-1}$, $b_i \geq e_{i-1}$. 

	Étant donné un n\oe{}ud et un temps de départ $t_0$, il existe alors trois types de chemins pertinents : le plus court est celui qui minimise le nombre de liens, le premier est celui qui arrive le plus tôt dans le temps (c'est-à-dire qui minimise $t_n - t_0$), et le plus rapide est celui qui minimise $t_n - t_1$, c'est-à-dire le temps effectivement passé sur le chemin~\sidenote{Ces définitions ne sont pas exclusives; un chemin peut être le premier, le plus court et le plus rapide, ou n'importe quelle combinaison de ces $3$ possibilités}. La figure~\ref{fig:chemins} offre une illustration de ces notions différentes. Ces définitions ont été introduites par~\cite{holme2012temporal}.

	\begin{figure}
		\includegraphics[width=1\linewidth]{paths}
		\caption{}
		\label{fig:chemins}
	\end{figure}

	\subsubsection{Connexité}

	% connectedness


	%\subsubsection{Centralités}
	% centralities
	Il est naturel de vouloir évaluer l'importance d'un n\oe{}ud au cours du temps. Dans la théorie des graphes, les notions de centralité~\cite{betweenness, closeness} sont deux métriques à cet effet. Nous étendons les notions de \emph{centralité de proximité} d'un n\oe{}ud $v\in V$ comme la moyenne de l'inverse de la durée du premier chemin entre tous les n\oe{}uds du flot et $v$ ~\sidenote{Autrement dit, du temps moyen d'accès à $v$ depuis tous les autres n\oe{}uds}. Nous étendons la \emph{centralité d'intermédiarité} d'un n\oe{}ud $v\in V$ comme étant la fraction des plus courts et plus rapides chemins impliquant $v$ au temps $t$, pour tous les temps $t\in T$.

	\subsection{Énumération des cliques maximales d'un flot de liens}
% Cliques
Certaines notions définies, par exemple celle de cliques dans un flot de liens, demandent un travail algorithmique pour être calculées. Nous définissons un algorithme glouton permettant d'énumérer toutes les cliques maximales d'un flot de liens $L=(T,V,E)$.

Son principe est le suivant : l'algorithme initialise une file $S$ avec les cliques triviales du flot de liens, c'est-à-dire $(\{u,v\}, [b,b])$ pour tout $(b,e,u,v)\in E$. Ensuite, l'algorithme prend un élément $(X, [b,e])$ de $S$, et cherche si il existe un n\oe{}ud $x\not\in X$ tel que $(X\cup\{x\}, [b,e])$ est une clique, ou un temps $e'>e$ tel que $(X, [b,e'])$ est une clique. Si l'un de ces deux conditions est remplie, la nouvelle clique est ajoutée à $S$, sinon la clique est maximale. Lorsque la file $S$ est vide, toutes les cliques maximales du flot ont été énumérées. 

Nous avons adapté cet algorithme aux cas des liens instantanés (où $b=e$), qui énumère alors toutes les $\Delta$-cliques d'un flot pour un $\Delta$ donné. L'algorithme a une complexité au pire cas en ${\cal O}(2^nm^2n^3 + 2^nm^3n^2)$ temps et ${\cal O}(2^nnm^2)$ espace. Nous avons prouvé et implémenté cet algorithme, puis nous avons énuméré toutes les $\Delta$-cliques maximales du jeu de données de contacts entre étudiants de classe préparatoire Thiers-Highschool~\cite{sociopatterns2008}. La figure~\ref{fig:cliques-highschool} montre deux de ces $\Delta$-cliques maximales dans le flot de liens, pour $\Delta=60$ secondes.

\begin{figure*}
	\includegraphics[height=225px]{5-824_690_807_658_804-1353325660_1353325820}
	\includegraphics[height=225px]{2-1613_1672-1353920500_1353921480}
	\caption{titre.}
	\label{fig:cliques-highschool}
\end{figure*}

\newpage
\section{Application à l'analyse de trafic IP}

% Contexte IP

% Données
	\subsection{Données}

	Afin de valider notre approche, nous avons choisi le dataset MAWI~\cite{mawilab2008}. Le projet MAWI collecte des traces de trafic IP sur le réseau WIDE~\sidenote{L'un des réseaux académiques japonais, comparable à RENATER en France.} depuis 1999. Chaque jour, 15 minutes de trafic sont collectées, anonymisées et rendues publiques. Certaines traces plus longues ~-- allant jusqu'à 83 heures ~-- ont été collectées dans le cadre du projet "A Day in the Life of the Internet". Étant donné la nature de la capture, il est intéressant de noter que le réseau résultant est intrinsèquement biparti : le trafic entre deux machines de WIDE ne passe jamais sur la machine de capture, de même que le trafic entre deux machines extérieures à WIDE. Autrement dit, on n'observe sur ce point de capture que les échanges entre des machines de WIDE et Internet.

	Les données se présentent sous la forme d'une séquence de paquets IP dont l'on extrait les adresses IP ainsi que l'horodatage, soit une séquence de $(t,u,v)$ signifiant que les machines $u$ et $v$ ont interagi au temps $t$. Afin d'étudier cette séquence comme un flots de liens, nous choisissons une valeur $\Delta = 2$ secondes dans un premier temps. Autrement dit, nous ne faisons aucune différence entre ce qui arrive à $t$ ou $1$ seconde avant ou après $t$. Choisir les valeurs les plus appropriées pour $\Delta$ sort du contexte de cette thèse. 

	Par ailleurs, le groupe de travail MAWI fourni une base de données publiques en ligne d'événements, \emph{MAWILab}. Les événements signalés sont détectés de façon automatique et non supervisée par $4$ détecteurs. Des travaux plus récents de~\cite{mazel} proposent une taxonomie de plus de 100 catégories révélant précisément la nature des anomalies dans le trafic. À ce titre, la base de données d'événements \emph{MAWILab}, sans constituer une vérité de terrain, facilite le travail de quiconque souhaitant détecter des événements au sein des traces de trafic MAWI.

	\subsection{Adaptation aux flots bipartis}

	De par la nature de la capture de trafic, la structure induite par les données MAWI est intrinsèquement bipartie, c'est-à-dire que l'on observe deux sous ensemble disjoints de machines, tous les échanges étant entre l'un et l'autre ensemble. Si l'on modélise la trafic par un flot de liens $L = (T,V,E)$, cela veut dire que l'ensemble $V$ peut être séparé en deux sous ensembles $\top$ et $\bot$ tels que $\top\cup\bot = V$ et $\top \cap \bot = \emptyset$, et que les éléments de $E$ sont des liens $(b,e,u,v)$, avec $u\in\top$ et $v\in\bot$. Un flot de liens biparti est un triplet $L = (T, \top, \bot, E)$, avec $T=[\alpha,\omega]$ un intervalle de temps, $\top$ et $\bot$ deux ensembles de n\oe{}uds disjoints, et $E\subseteq T\times T\times \top \times \bot$ l'ensemble des liens. 

	L'impact de la biparticité sur la structure des échanges est très forte, et nécessite d'adapter nos notions, ainsi que d'en définir de nouvelles.

	Par exemple, la densité quantifie le nombre de liens existants par rapport aux nombre de liens possibles. Dans le cas d'un réseau biparti, il y a moins de liens possibles (puisqu'on ne peut pas avoir de lien $(b,e,u,v)$ avec $u,v\in\bot$, notamment). Il faut modifier la normalisation : 
	\begin{equation}
		\delta(L) = \frac{2\cdot\sum_{(b,e,u,v)\in E}e-b}{|\top|\cdot|\bot|\cdot (\omega-\alpha)}
	\end{equation}

	Il s'agit bien de la probabilité, lorsque l'on prend un temps $t$ au hasard, un n\oe{}ud $u$ dans $\top$ et un n\oe{}ud $v$ dans $\bot$ au hasard, qu'il existe un lien entre $u$ et $v$ au temps $t$ dans le flot.

	Dans un flot de liens biparti, nous définissons le coefficient de clustering d'un n\oe{}ud $v$ comme la probabilité, lorsque l'on choisit trois n\oe{}uds $x,y,z$ et un temps $t$ au hasard tels qu'il existe des liens entre $v$ et $x$, $x$ et $y$, $y$ et $z$ au temps $t$, qu'il existe également un lien entre $v$ et $z$ au temps $t$. Il s'agit de la densité du cluster de n\oe{}uds défini par les voisins de $v$ ainsi que les voisins de ces voisins, soit ${\cal C}(v) = \{(u,t) : \exists (b,e,u,v) \in E, t\in [b,e]\}\cup
		\{(u,t): \{(b,e,v,x), (b',e',u,x)\}\subseteq E, t\in [b',e']\cap [b,e]\}$. On a donc $cc(v) = \delta({\cal C}(v))$, et le clustering du flot est défini comme la moyenne des coefficients de clustering des n\oe{}uds : 

		\begin{equation}
			cc(L)  = \frac{\sum_{v\in V}cc(v)}{|V|}
		\end{equation}

	Malgré l'adaptation de certaines notions aux flots de liens bipartis, l'impact de la structure bipartie sur les résultats obtenus est difficile à interpréter, et il peut être judicieux d'étudier un flot non biparti dérivé de celui-ci. Définie sur les graphes par~\cite{latapy}, nous étendons le projeté de graphe biparti aux flots de liens. 

	Étant donné un flot de liens biparti $L = (T,\top,\bot, E)$, nous définissons le $\bot$-projeté~\sidenote{Respectivement $\top$-projeté} $L_\bot$ de $L$ comme le flots de liens $L_\bot = (T, \bot, E_\bot$, avec $E_\bot = \{(b,e,u,v): \exists (b',e',u,x), (b'',e'',x,v)\in E, u,v\in \bot, x\in\top, [b',e']\cap[b'',e''] = [b,e], b > e\}$. Intuitivement, il y a un lien entre $u$ et $v$ dans $E_\bot$ si et seulement si $u$ et $v$ ont un voisin en commun dans $\top$ en même temps. La notion de projeté est illustrée en figure~\ref{fig:projection-graphbip}.

	\begin{figure}
		\includegraphics[width=0.95\linewidth]{projex-high-low}
		\label{fig:projex-high-low}
		\caption{Projeté de flot de liens.}
	\end{figure}

	Nous adaptons également la notion de redondance, définie sur les graphes bipartis; nous ne la détaillons pas dans le cadre de ce résumé.

	% Méthode evt
	\subsection{Notre approche de caractérisation d'événements}

	Dans le trafic IP, les événements sont présents à des échelles de temps et structure très variables. Par ailleurs, on peut s'attendre à ce qu'un événement très global aie un impact sur de nombreuses métriques, tandis que des événements plus subtils requièrent des métriques plus précises pour être détectés ou caractérisés. Enfin, notre intuition est que les événéments à grande échelle écrasent les événéments plus petits, les rendant invisibles.

	Nous définissons trois catégories d'événements, de la plus générale à la plus spécifique : un événement \emph{détecté} est simplement une anomalie dans une distribution de valeurs, un événement \emph{expliqué} est un événement pour lequel nous sommes capables d'identifier (au moins) une cause dans les données; enfin, un événement \emph{interprété} est un événément pour lequel nous avons une interprétation précise, qui est éventuellement externe aux données.

	L'approche que nous proposons est la suivante : nous calculons des métriques simples sur la donnée~\sidenote{Par exemple, le nombre de paquets par seconde, ou le nombre d'IPs distinctes par seconde.}, puis identifions visuellement les anomalies statistiques sur les distributions de ces valeurs. Ensuite, nous tentons d'expliquer chaque anomalie statistique. Si nous sommes en mesure d'expliquer une anomalie, nous la retirons aussi précisément que possible de la donnée; sinon, nous la laissons dans la donnée, en comptant sur une métrique ultérieure pour l'expliquer. Nous recommencons ensuite le processus avec des métriques plus complexes~\sidenote{Par exemple le degré dans le flot, ou la densité d'un sous flot, ou le coefficient de clustering.}. 

	En répétant ce processus avec une connaissance parfaite du trafic, nous serions capable d'obtenir un trafic dépourvu d'événements. Bien sûr, il s'agit d'un cas asymptotique difficile, voire impossible à atteindre en réalité, mais un trafic normal~\sidenote{C'est-à-dire sans événement qui ne soit pas légitime.} est un point de comparaison utilisé par de nombreuses méthodes dans la littérature.  


	\subsection{Résultats sur les données MAWI}

	Nous appliquons la méthode définie en section XX aux données MAWI présentées précédemment. Dans le cadre de ce résumé, nous ne détaillerons les résultats que pour une statistique, et discuterons brièvement des résultats obtenus, qui sont détaillés dans le manuscrit de thèse. 

	Nous montrons en figure~\ref{fig:pktspersecond} gauche le nombre de paquets par seconde, c'est-à-dire que pour chaque seconde $s$, nous comptons le nombre d'éléments $(t,u,v)$ tels que $s \leq t < s +1$. On peut constater que le nombre de paquets oscille autour d'une valeur moyenne de $45,000$ paquets par seconde, avec quelques pics, par exemple autour de la seconde $130$.

	\begin{figure}
		\includegraphics[angle=-90,width=0.45\linewidth]{20130625_1h-pktspersecond}
		\includegraphics[angle=-90,width=0.45\linewidth]{20130625_1h-pktspersecond}
		\caption{Number of packets per second over time, before and after removal of explained events.}
		\label{fig:pktspersecond}
	\end{figure}

	La figure~\ref{pktspersecond-dists} montre la distribution et la distribution cumulative inverse de ces valeurs. La distribution cumulative inverse en axes linéaires permet d'identifier visuellement une distribution normale avec quelques valeurs extrêmes. Sur la distribution cumulative inverse en axes linéaire - logarithmique, on voit un saut dans la distribution des valeurs à $100,000$, et nous choisissons cette valeur comme seuil : toutes les secondes contenant plus de $100,000$ paquets sont considérées comme des anomalies. Nous obtenons ainsi quatre points, dont $3$ sont situés dans l'intervalle $[130,150]$ et $1$ dans l'intervalle $[1530, 1531]$. 

	\begin{figure*}
		\includegraphics{20130625_short_pkt-pktspersecond-dists}
		\caption{Distributions for the number of packets per second.}
		\label{fig:pktspersecond-dists}
	\end{figure*}

	% Interp.
	Nous essayons à présent d'expliquer les événements détectés sur les distributions. Nous observons le nombre de paquets échangés par paire de n\oe{}uds dans l'intervalle $[130,150]$, et constatons que plus de 80\% des paquets échangés sur cet intervalle le sont entre deux machines. Nous retirons donc de la donnée tous les paquets entre les deux machines sur l'intervalle $[130,150]$, et recalculons le nombre de paquets par seconde, visible en figure~\ref{fig:pktspersecond} droite. On peut constater que le pic a disparu. 

	Appliquer le même raisonnement à l'intervalle $[1530,1531]$ ne permet pas de trancher de façon satisfaisante. Le total de paquets est réparti équitablement entre plusieurs paires d'IPs, sans qu'aucune ne se détache clairement. 

	% Conclu et résultats autres
	Nous appliquons notre méthode à d'autres statistiques, plus subtiles, et sommes en mesure de répérer des événements à l'aide de notre formalisme. Certains de ces événements sont présents dans la base de données \emph{MAWILab}, d'autres ne le sont pas car considérés comme du traffic légitime (ce sont des événéments mais pas des anomalies); notre approche statistique ne fait pas de différence entre ces deux sémantiques différentes.

\newpage

\section{Conclusion}

Au travers de cette thèse, nous avons posé les bases d'un formalisme décrivant les séquences d'interactions, que nous modélisons par des \emph{flots de liens}. Nous avons montré qu'il existe des notions naturelles de densité, de voisinage, de chemin qui étendent efficacement la théorie des graphes en prenant en compte à la fois les aspects temporels et structurels liés aux flux d'interactions.

Concernant les aspects algorithmiques, nous prenons l'exemple des cliques afin de proposer un premier algorithme glouton d'énumération de cliques maximales d'un flot de liens. Nous mettons l'implémentation de cet algorithme en pratique sur des données de contact entre étudiants de classe préparatoire, et montrons que les cliques maximales du flot de liens permettent d'identifier des sous-flots pertinents dans la donnée. 

Nous avons appliqué notre formalisme à l'analyse de trafic IP, sur les données fournies par le groupe de travail MAWI. Nous proposons une méthode d'identification d'événement permettant de nettoyer le flot de liens de ses événements, et d'obtenir un terme un flot de liens représentant un comportement normal.

L'une des contributions de cette thèse est le nombre important de perspectives qu'elle ouvre. Qu'il s'agisse de visualisation de flots, de compression, de détection de groupes denses, de défis algorithmiques, de prédiction de liens ou encore de recommandation.
