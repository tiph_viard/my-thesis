\documentclass[usletter,justified,nobib,marginals=raggedright]{tufte-book}
% \documentclass[a4paper,11pt]{book}

\hypersetup{colorlinks}% uncomment this line if you prefer colored hyperlinks (e.g., for onscreen viewing)

%%
% Book metadata
\title{Link streams for modelling interactions over time and application to the analysis of IP traffic}
\author[T. Viard]{Tiphaine Viard}


% \usepackage{setspace}
% \doublespacing
%%
% If they're installed, use Bergamo and Chantilly from www.fontsite.com.
% They're clones of Bembo and Gill Sans, respectively.
%\IfFileExists{bergamo.sty}{\usepackage[osf]{bergamo}}{}% Bembo
%\IfFileExists{chantill.sty}{\usepackage{chantill}}{}% Gill Sans

\tolerance 9999 \emergencystretch 3em\relax

%\usepackage{microtype}
\usepackage[noend]{algpseudocode}

\errorcontextlines\maxdimen
% begin vertical rule patch for algorithmicx (http://tex.stackexchange.com/questions/144840/vertical-loop-block-lines-in-algorithmicx-with-noend-option)
\makeatletter
% start with some helper code
% This is the vertical rule that is inserted
\newcommand*{\algrule}[1][\algorithmicindent]{\makebox[#1][l]{\hspace*{0.3em}\vrule height .75\baselineskip depth .25\baselineskip}}%

\newcount\ALG@printindent@tempcnta
\def\ALG@printindent{%
		\ifnum \theALG@nested>0% is there anything to print
		\ifx\ALG@text\ALG@x@notext% is this an end group without any text?
		% do nothing
		\addvspace{-1.35pt}% FUDGE for cases where no text is shown, to make the rules line up
		\else
		\unskip
		% draw a rule for each indent level
		\ALG@printindent@tempcnta=1
		\loop
		\algrule[\csname ALG@ind@\the\ALG@printindent@tempcnta\endcsname]%
		\advance \ALG@printindent@tempcnta 1
		\ifnum \ALG@printindent@tempcnta<\numexpr\theALG@nested+1\relax% can't do <=, so add one to RHS and use < instead
		\repeat
		\fi
		\fi
}%

\usepackage{etoolbox}

% the following line injects our new indent handling code in place of the default spacing

\patchcmd{\ALG@doentity}{\noindent\hskip\ALG@tlm}{\ALG@printindent}{}{\errmessage{failed to patch}}

\makeatother
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{algorithm}
\usepackage{textcomp}
\usepackage{lineno,hyperref}
\usepackage{array}
\usepackage{geometry}
\usepackage{booktabs,ragged2e}
\usepackage{pdflscape}


\usepackage[dvipsnames]{xcolor}

\usepackage{fancyvrb}

% redefine \VerbatimInput
\RecustomVerbatimCommand{\VerbatimInput}{VerbatimInput}%
{fontsize=\footnotesize,
 %
 frame=lines,  % top and bottom rule only
 framesep=2em, % separation between frame and text
 rulecolor=\color{Gray},
 %
 label=\fbox{\color{Black}Example of IP traffic from the MAWI dataset},
 labelposition=topline,
 %
 commandchars=\|\(\), % escape character and argument delimiters for
                      % commands within the verbatim
 commentchar=*        % comment character
}

\newcolumntype{P}[1]{>{\centering\arraybackslash}p{#1}}
\newcolumntype{M}[1]{>{\centering\arraybackslash}m{#1}}

\renewcommand{\algorithmicrequire}{\textbf{Input: A link stream $L$ defined over a set of nodes $V$, a duration $\Delta$}}
\renewcommand{\algorithmicensure}{\textbf{Output: The set of all maximal \dclique{}s in $L$}}
\newtheorem{lemma}{Lemma}
\newtheorem{theorem}{Theorem}

%%%% New commands
\newcommand{\noteperso}[1]{\fbox{\begin{minipage}{\textwidth}
			#1
		\end{minipage}}}
\newcommand{\dclique}{$\Delta$-clique}
\newcommand{\demidelta}{\frac{\Delta}{2}}
\newcommand{\clique}[3]{\ensuremath{(#1,[#2,#3])}}

\newcommand{\ie}{{\em i.e.}}

\newcommand{\deltaext}{\ensuremath{\overline{\delta}}}
\newcommand{\degreeext}{\ensuremath{\overline{d}}}

\newcommand{\sublinkeq}{\subseteq}
\newcommand{\substreameq}{\subseteq}

\newcommand{\distance}{d}
%\newcommand{\latency}{\mathcal{L}}
\newcommand{\latency}{\ell}
\newcommand{\timetoreach}{\mathcal{T}}
\newcommand{\closeness}{\mathcal{C}}
\newcommand{\betweenness}{\mathcal{B}}


\newcommand{\Cuv}{|C(u,v)|}
\newcommand{\Cu}{|C(u)|}
\newcommand{\Cv}{|C(v)|}
\newcommand{\uinterv}{|C(u) \cap C(v) |}

\newcommand{\nC}{n(C)}
\newcommand{\mC}{m(C)}
\newcommand{\dC}{\delta(C)}
\newcommand{\compC}{comp(C)}

\newcommand{\linegraph}{\widehat}

%%
% Just some sample text
\usepackage{lipsum}
\usepackage[utf8]{inputenc}

%%
% For nicely typeset tabular material
\usepackage{booktabs}
\usepackage{minitoc}
%%
% For graphics / images
\usepackage{graphicx}
\setkeys{Gin}{width=\linewidth,totalheight=\textheight,keepaspectratio}
\graphicspath{{graphics/}}

% The fancyvrb package lets us customize the formatting of verbatim
% environments.  We use a slightly smaller font.
\usepackage{fancyvrb}
\usepackage{dsfont}
\fvset{fontsize=\normalsize}

%%
% Prints argument within hanging parentheses (i.e., parentheses that take
% up no horizontal space).  Useful in tabular environments.
\newcommand{\hangp}[1]{\makebox[0pt][r]{(}#1\makebox[0pt][l]{)}}

%%
% Prints an asterisk that takes up no horizontal space.
% Useful in tabular environments.
\newcommand{\hangstar}{\makebox[0pt][l]{*}}

%%
% Prints a trailing space in a smart way.
\usepackage{xspace}
\usepackage{subfig}

% Prints the month name (e.g., January) and the year (e.g., 2008)
\newcommand{\monthyear}{%
  \ifcase\month\or January\or February\or March\or April\or May\or June\or
  July\or August\or September\or October\or November\or
  December\fi\space\number\year
}

% Prints an epigraph and speaker in sans serif, all-caps type.
\newcommand{\openepigraph}[2]{%
  %\sffamily\fontsize{14}{16}\selectfont
  \begin{fullwidth}
  \sffamily\large
  \begin{doublespace}
  \noindent\allcaps{#1}\\% epigraph
  \noindent\allcaps{#2}% author
  \end{doublespace}
  \end{fullwidth}
}

\usepackage{units}

% Typesets the font size, leading, and measure in the form of 10/12x26 pc.
\newcommand{\measure}[3]{#1/#2$\times$\unit[#3]{pc}}

% Macros for typesetting the documentation
\newcommand{\hlred}[1]{\textcolor{Maroon}{#1}}% prints in red
\newcommand{\hangleft}[1]{\makebox[0pt][r]{#1}}
\newcommand{\hairsp}{\hspace{1pt}}% hair space
\newcommand{\hquad}{\hskip0.5em\relax}% half quad space
\newcommand{\TODO}{\textcolor{red}{\bf TODO!}\xspace}
\newcommand{\eg}{\textit{e.\hairsp{}g.}\xspace}
\newcommand{\na}{\quad--}% used in tables for N/A cells
\providecommand{\XeLaTeX}{X\lower.5ex\hbox{\kern-0.15em\reflectbox{E}}\kern-0.1em\LaTeX}
\newcommand{\tXeLaTeX}{\XeLaTeX\index{XeLaTeX@\protect\XeLaTeX}}
% \index{\texttt{\textbackslash xyz}@\hangleft{\texttt{\textbackslash}}\texttt{xyz}}
\newcommand{\tuftebs}{\symbol{'134}}% a backslash in tt type in OT1/T1
\newcommand{\doccmdnoindex}[2][]{\texttt{\tuftebs#2}}% command name -- adds backslash automatically (and doesn't add cmd to the index)
\newcommand{\doccmddef}[2][]{%
  \hlred{\texttt{\tuftebs#2}}\label{cmd:#2}%
  \ifthenelse{\isempty{#1}}%
    {% add the command to the index
      \index{#2 command@\protect\hangleft{\texttt{\tuftebs}}\texttt{#2}}% command name
    }%
    {% add the command and package to the index
      \index{#2 command@\protect\hangleft{\texttt{\tuftebs}}\texttt{#2} (\texttt{#1} package)}% command name
      \index{#1 package@\texttt{#1} package}\index{packages!#1@\texttt{#1}}% package name
    }%
}% command name -- adds backslash automatically
\newcommand{\doccmd}[2][]{%
  \texttt{\tuftebs#2}%
  \ifthenelse{\isempty{#1}}%
    {% add the command to the index
      \index{#2 command@\protect\hangleft{\texttt{\tuftebs}}\texttt{#2}}% command name
    }%
    {% add the command and package to the index
      \index{#2 command@\protect\hangleft{\texttt{\tuftebs}}\texttt{#2} (\texttt{#1} package)}% command name
      \index{#1 package@\texttt{#1} package}\index{packages!#1@\texttt{#1}}% package name
    }%
}% command name -- adds backslash automatically
\newcommand{\docopt}[1]{\ensuremath{\langle}\textrm{\textit{#1}}\ensuremath{\rangle}}% optional command argument
\newcommand{\docarg}[1]{\textrm{\textit{#1}}}% (required) command argument
\newenvironment{docspec}{\begin{quotation}\ttfamily\parskip0pt\parindent0pt\ignorespaces}{\end{quotation}}% command specification environment
\newcommand{\docenv}[1]{\texttt{#1}\index{#1 environment@\texttt{#1} environment}\index{environments!#1@\texttt{#1}}}% environment name
\newcommand{\docenvdef}[1]{\hlred{\texttt{#1}}\label{env:#1}\index{#1 environment@\texttt{#1} environment}\index{environments!#1@\texttt{#1}}}% environment name
\newcommand{\docpkg}[1]{\texttt{#1}\index{#1 package@\texttt{#1} package}\index{packages!#1@\texttt{#1}}}% package name
\newcommand{\doccls}[1]{\texttt{#1}}% document class name
\newcommand{\docclsopt}[1]{\texttt{#1}\index{#1 class option@\texttt{#1} class option}\index{class options!#1@\texttt{#1}}}% document class option name
\newcommand{\docclsoptdef}[1]{\hlred{\texttt{#1}}\label{clsopt:#1}\index{#1 class option@\texttt{#1} class option}\index{class options!#1@\texttt{#1}}}% document class option name defined
\newcommand{\docmsg}[2]{\bigskip\begin{fullwidth}\noindent\ttfamily#1\end{fullwidth}\medskip\par\noindent#2}
\newcommand{\docfilehook}[2]{\texttt{#1}\index{file hooks!#2}\index{#1@\texttt{#1}}}
\newcommand{\doccounter}[1]{\texttt{#1}\index{#1 counter@\texttt{#1} counter}}

% Generates the index
\usepackage{makeidx}
\makeindex

% Where to look for figures
\graphicspath{{Logo/}{Figures/}{Chapters/Figures/}} 


\AtBeginDocument{%
\fontsize{12}{16}\selectfont
 }%

\usepackage[toc,page]{appendix}

%%%%%%%%% Complenet

\newcommand{\com}[1]{{\color{red}{#1}}}

\newcommand{\neutral}[1]{\todo[color=blue!40]{#1}}
\newcommand{\good}[1]{\todo[color=green!40]{#1}}
\newcommand{\bad}[1]{\todo[color=red!40]{#1}}
\newcommand{\neutraln}[1]{\todo[color=blue!40,noline]{#1}}
\newcommand{\goodn}[1]{\todo[color=green!40,noline]{#1}}
\newcommand{\badn}[1]{\todo[color=red!40,noline]{#1}}

\newcommand{\debianml}{Debian mailing list}
\newcommand{\ml}{mailing-list}


%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%% NetSciCom
\hyphenation{}

\newcommand{\myscale}{0.99}
\newcommand{\dclustering}{$\Delta$-clustering coefficient}
\newcommand{\tclustering}{$\tau$-clustering coefficient}
\newcommand{\dcc}{\ensuremath{\Delta\mbox{-cc}}}
\newcommand{\ddensity}{$\Delta$-density}
\newcommand{\dd}{\ensuremath{\delta_\Delta}}
\newcommand{\llfloor}{\ensuremath{\left\lfloor}}
\newcommand{\rrfloor}{\ensuremath{\right\rfloor}}
\newcommand{\llceil}{\ensuremath{\left\lceil}}
\newcommand{\rrceil}{\ensuremath{\right\rceil}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\DeclareUnicodeCharacter{00A0}{~}

\begin{document}

\setcounter{secnumdepth}{2}

% Front matter
\frontmatter


% % - - - - - - - début de la page 
\thispagestyle{empty}

%\includegraphics[scale=0.12]{upmc-logo.png}
\begin{fullwidth}
		\begin{center}
			\includegraphics[width=0.3\linewidth]{logo_upmc}
		\end{center}
		\vspace*{0.5cm}
		
		\begin{center}
			
			\textbf{TH\`ESE}\\
			présentée pour obtenir le grade de\\
			\vspace*{0.5cm}
			\begin{center}\textsc{Docteur de l'université Pierre et Marie Curie}\end{center} \ \\
			
			\vspace*{0.5cm}
			
			Sp\'ecialit\'e \textsc{Informatique}\ \\ 
			
			\vspace*{0.5cm}
			
			École doctorale Informatique, Télécommunications et Électronique (Paris)
			\vspace*{1.5cm}
			
		\end{center}
		
			% Thesis title
			\hrule height 2px
			\vspace*{0.5cm}
			{\huge\centering \textsc{Flots de liens pour la mod\'{e}lisation d'interactions temporelles et application \`{a} l'analyse de trafic IP\\ }}
			\vspace*{0.5cm}
			\hrule height 2px
			\vspace*{1.5cm}
			\begin{center}
			{\huge Tiphaine \textsc{VIARD}}
			\end{center}

			\vspace*{1cm} 
			\flushleft{Soutenue publiquement le 28 septembre 2016 devant le jury composé de}\\[2ex]
			\flushleft{\begin{tabular}{llll}
					\emph{Rapporteurs:} &  Pierre \textsc{Borgnat} & Chargé de recherches, \textsc{CNRS}\\
					 &  Olivier \textsc{Festor} & Directeur de recherches, Inria\\
					 \emph{Examinateurs:} &  Arnaud \textsc{Casteigts} & Maître de conférences, Université de Bordeaux\\
					 &  Marcelo \textsc{Dias de Amorim} & Directeur de recherches, \textsc{CNRS} \\
					 &  Philippe \textsc{Owezarski} & Directeur de recherches, \textsc{CNRS}\\
					 &  Véronique \textsc{Serfaty} & Responsable scientifique, DGA  \\
					 \emph{Directeurs:} &  Clémence \textsc{Magnien} & Directrice de recherches, \textsc{CNRS}\\
					 &  Matthieu \textsc{Latapy} & Directeur de recherches, \textsc{CNRS}\\
				\end{tabular}
			}
				
			% }
% % - - - - - - - fin de la page de garde

\cleardoublepage
\thispagestyle{empty}
\vspace*{\fill}
\begin{flushright}
\emph{
{\Large "Be who you are and say what you feel,\\ because those who mind don't matter,\\ and those who matter don't mind."}}\\
~-- Bernard M. Baruch
\end{flushright}
\vspace*{\fill}
\cleardoublepage

\input{acknowledgments.tex}


\bigskip
\cleardoublepage

% contents
\dominitoc
\tableofcontents
\end{fullwidth}
%\listoffigures

%\listoftables

%%
% Start the main matter (normal chapters)
\mainmatter
% \include{resume2}

% chapters
\include{Chapters/chapter1}
\include{Chapters/ls-basics}
\include{Chapters/ls-density-based}
\include{Chapters/ls-path-based}
\include{Chapters/delta-analysis}
\include{Chapters/chapter3}
\include{Chapters/chapter4}
\include{Chapters/chapter5}

%%
% The back matter contains appendices, bibliographies, indices, glossaries, etc.

% \backmatter
\begin{appendices}
\include{Chapters/appendixA}
\include{Chapters/appendixB}
\end{appendices}

\bibliography{thesis}
% \bibliographystyle{plain}
\bibliographystyle{apalike}

\printindex

\end{document}

