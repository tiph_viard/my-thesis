\chapter{$\Delta$-analysis of link streams}
\label{cha:ls:delta-analysis}

In many contexts, the data is a sequence of instantaneous contacts $(t,u,v)$ rather than of  contacts with duration $(b,e,u,v)$. This is the case, for example, of face-to-face contacts captured by sensors, or IP traffic studied as a sequence of packets.

However, the notions we have defined in Chapter~\ref{cha:ls:density} do not take into account such instantaneous links. The density, for instance, sums the durations of links: if links have duration $0$, they account for nothing in the density. To avoid this, one generally considers a time scale $\Delta$ and defines $\Delta$-related notions like the $\Delta$-density, the $\Delta$-degree, and so on. In this chapter, we define a transformation of a link stream to study it at a given scale $\Delta$, and show the notions defined in Chapter~\ref{cha:ls:density} are equivalent to $\Delta$-related notions. 

\section{$\Delta$-analysis and instantaneous links}

%\noteperso{PLAN de section : topo sur la problématique ; def delta-quantités ; def Ldelta ; equivalence pour densite et degres ; equivalence pour les clusters et partitions ; chemins ; discussion (intérêt, généralisation)}

When studying a link stream $L$, one often wants to consider a time scale $\Delta$ coarser than the exact times of links in $L$.
For instance, if $L$ represents contacts between individuals, one may be interested in knowing if these individuals generally meet every day, every week or every year.

In particular, some data are typically in the form of instantaneous links, for example IP traffic: a packet exchanged between two machines $u$ and $v$ occurs exactly at a given time $t$.  

One may see the parameter $\Delta$ as capturing the following intuition: if a link exists between two nodes within a time interval of duration at most $\Delta$ then one considers that the two nodes are continuously linked together during this time interval.

Given a link stream $L=(T,V,E)$ and a value $\Delta \in [0,\omega-\alpha]$, we define $L_\Delta = (T_\Delta,V,E_\Delta)$ as the link stream such that $T_\Delta = [\alpha+\demidelta, \omega-\demidelta]$ and $E_\Delta = (\{(b',e',u,v): \exists (b,e,u,v)\in E, [b',e'] = [b-\demidelta,e+\demidelta] \cap T_\Delta \})$. In other words, any two nodes are linked together at time $t$ in $L_\Delta$ if and only if they are linked together in $L$ at a time $t'$ such that $t \in [t'-\demidelta,t'+\demidelta] \cap T_\Delta$. This operation is depicted in Figure~\ref{fig:delta-transform}.%~\sidenote{The core idea is that one has to add $\Delta$ time around the link}.

\begin{figure}
	\includegraphics[width=1\linewidth]{delta-transform}
	\caption{$\Delta$-transformation of the link stream $L=([0,10], \{a,b,c\}, \{(1,2,a,b),(3,4,b,c),\dots\}$ for $\Delta=2$. $L_\Delta = ([1,9],\{a,b,c\} , \{(1,3,a,b),(2,5,b,c),\dots\})$.}
	\label{fig:delta-transform}
\end{figure}


\section{Density}

~\cite{viard2014identifying} define $\delta_\Delta(L)$ as the probability, when one takes a random time interval of size $\Delta$ in $T$ and two random nodes $u$ and $v$ in $V$ that these two nodes are linked at some time during this time interval. %More formally:
% $$
% \delta_\Delta(L) = \frac{\left|\bigcup_{(t,u,v)\in E}[t, t+\Delta]\right|}{|T_\Delta}
% $$
This is equivalent to the probability, when one takes a random time instant $t$ in $[\alpha+\demidelta, \omega-\demidelta]$, that the two nodes are linked at some time in $[t-\demidelta,t+\demidelta]$ in $L$.
By definition of $L_\Delta$, this is equivalent to the probability, when one takes a time $t$ in $T_\Delta$, that the two nodes are linked at time $t$ in $L_\Delta$: if the nodes are linked together at some time in $[t-\demidelta,t+\demidelta]$ in $L$ then they are linked together at time instant $t$ in $L_\Delta$, and conversely.
The probability that this occurs is nothing but the density of $L_\Delta$. Then, we have $\delta_\Delta(L) = \delta(L_\Delta)$.

\section{Degree}

We define the $\Delta$-degree of a stream $d_\Delta(L)$ as the number of expected neighbors of a node $u$ chosen at random when one takes a random interval of size $\Delta$ in $T$. %Formally:

% $$
% 		d_\Delta(u) = \frac{1}{\omega - \alpha - \Delta}\sum_{u\in V} \left|\bigcup_{(t,u,v)\in E}[t,t+\Delta]\right|
% $$
% and the $\Delta$-degree of $L$ is the average $\Delta$-degree for all nodes:
% $$
% 	d_\Delta(L) = \frac{1}{n}\sum_{u\in V}d_\Delta(u)
% $$

We then have the following relation between $\Delta$-degree and $\Delta$-density: $\delta_\Delta(L) = \frac{d_\Delta(L)}{n-1}$.
As we have shown in Chapter~\ref{cha:ls:density}, for any link stream $L'$, $\delta(L') = \frac{d(L')}{n-1}$. Since the order of $L$ is equal to the order of $L_\Delta$, we conclude that $d_\Delta(L) = d(L_\Delta)$.

\section{Clusters}

One can associate to any cluster $C$ a cluster $C_\Delta$, which is the result of the same operation as the one transforming a stream $L$ into $L_\Delta$. However, by doing so the clusters of $L_\Delta$ do not form a partition of the stream, even if they formed a partition of $L$, and density and degree are not equivalent to the $\Delta$-density and the $\Delta$-degree.

To circumvent this, one can define $\Delta$-clusters: $C$ is a $\Delta$-cluster if, for all $u\in V(C)$, and for all maximal intervals $[b,e]$ of $C(u)$, there is no $(x,y,u,v)$ in $E$ such that $b\leq x < \frac{\Delta}{2}$ or $e \geq y > e - \frac{\Delta}{2}$.

In other words, for each node in the cluster, there is at least $\frac{\Delta}{2}$ time between two implications of the node in two different clusters.

In the case of $\Delta$-clusters, density and degree are respectively equivalent to $\Delta$-density and $\Delta$-degree.  

\section{Conclusion}

In this chapter, we show that our formalism makes it possible to study link streams at a given scale $\Delta$; we define a transformation of a link stream, and show that the notions of density, degree and clusters defined in Chapter~\ref{cha:ls:density} remain relevant on the transformed stream. 

In our definition, $\Delta$ is a constant for all links in $L$, but the way we transform the stream is flexible: it is possible to set a value of $\Delta$ that is different for each $(u,v)$, at different times, or that is a fraction of the duration of the link, for instance; $\Delta$ can also be fixed for each link $(b,e,u,v)$ using external knowledge.