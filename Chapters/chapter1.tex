% \section*{Introduction}
% \noteperso{intro très générale, idée de la thèse, plan global du manuscrit; reprendre des deux intros de partie -- et en profiter pour les dvp + ?}
% \begin{fullwidth}
% \newpage

\begin{center}
	{\sc\Huge Introduction}
\end{center}
\addcontentsline{toc}{chapter}{Introduction}

\bigskip

\newthought{Interactions are everywhere:} in the contexts of face-to-face contacts, emails, phone calls, IP traffic, online purchases, running code, and many others. Interactions may be directed, weighted, enriched with supplementary information, yet the baseline remains: in all cases, an interaction means that two entities $u$ and  $v$ interact together from time $b$ to time $e$: for instance, two individuals $u$ and $v$ meet from time $b$ to time $e$, two machines on a network start an IP session from time $b$ to time $e$~\sidenote{Or exchange a packet at time $b=e$.}, two persons $u$ and $v$ phone each other from time $b$ to time $e$, and so on.

Sequences of interactions have strong temporal and structural features, and have been widely studied in the past years. The current approach mostly consists in focusing on their structural ~\sidenote{Which entities interact with each other.} or temporal properties~\sidenote{When and how frequently do interactions occur.}, but combining both aspects remains challenging.

If one wants to focus on the structure of interactions, graph theory is the natural framework to do so; it provides notions such as node degree (their number of links), density (the extent at which all nodes are connected together), paths (series of links going from a node to another one), etc. This language forms the basis of network science.

If one wants to focus on the dynamics of interactions, signal processing captures time features perfectly well; one typically computes properties that may be quantified in consecutive time windows, such as the number of interactions that occur every day, the number of involved nodes, the duration of interactions, etc.

These approaches give much insight on the considered objects, and have a key advantage: graph theory and signal processing offer a huge number of advanced tools; yet, they do not capture the both structural and temporal nature of interactions over time, and induce important information losses.

Much effort has therefore been devoted to upgrade these approaches; however, these extensions are often intricate, address only one aspect of the question, still induce important losses of information.%, and do not really solve the question but rather shifts the problem.

In this thesis, we explore a new approach consisting in modelling interactions directly as link streams, \ie{} series of quadruplets $(b,e,u,v)$ meaning that $u$ and $v$ interacted from time $b$ to time $e$. We will develop the basis of the corresponding formalism in Part $1$.

\medskip
In order to guide and assess this fundamental work, we focus on the analysis of IP traffic. It is particularly important to us that we make both fundamental and applied progress: application cases should feed our theoretical thoughts, and formal tools are designed to have meaning on application cases in the most general way.

Attacks against online services, networks, information systems, as well as identity thefts, have annual costs estimated
in billions of euros. These attacks also have dramatic consequences on user trust and the reliability of services. The
techniques developed in the context of these malicious activities are of ever-growing complexity, and frequently rely
on subtle malware (viruses or worms). Given this context,
there is a critical need of methods and tools to fight against
attacks and malware diffusion, and to give an appropriate
answer to the major societal questions raised.

IP traffic is typically collected at one or many intermediary points in the network. A router is set up for capture,
and will keep a record of all packets going through it. One
obtains a sequence of packets, each of these packets meaning that machines $u$ and $v$ interacted through the router at a time $t$.

In Part 2 we apply our framework to the analysis of IP traffic, with the aim of assessing the relevance of link streams for describing IP traffic as well as finding events inside the traffic. We devise a method to identify events at different scales, and apply it to a trace of traffic from the MAWI dataset. The high volume of traffic~\sidenote{Many thousands of packets each second.}, even over short periods of time, also makes it an ideal case of application to test the scalability of our approach.

\medskip
The work presented in this manuscript opens numerous perspectives; we summarize our contributions and present some perspectives for future work in Part 3.

% \end{fullwidth}

\part{A basic language for link streams}
\label{cha:flots-de-liens}

\begin{fullwidth}
\minitoc
\end{fullwidth}

\section*{Introduction}
	% Ici, parler plutôt de cohérence du framework, du besoin de consensus ?
We model sequences of interactions as sequences of quadruplets $(b,e,u,v)$, meaning that entities $u$ and $v$ have interacted from time $b$ to time $e$.

To study such interactions, one typically studies the properties of graphs $G_{t..t+\Delta}$ for a given $\Delta$ and for $t = 0,\ \Delta,\ 2\Delta$, etc. In other words, one splits time into consecutive slices of duration $\Delta$ and then studies the sequence of graphs obtained for each time slice. This leads to studies of how graph properties evolve with time. Others study the sequence of graphs $G_{0..t}$ for $t = \Delta,\ 2\Delta,\ \dots$ for a given $\Delta$. Such studies explore how graph properties evolve when the observation duration grows.

However, sequences of interactions are fundamentally different from dynamic graphs: if one were to stop time right now, some data retain meaning: for instance, the Web graph~\sidenote{A graph where nodes are web pages and one places a link between two pages if one cites the other.} can be frozen and studied at a given time $t$. In the case of interactions, there are typically few interactions at a given time~\sidenote{The graph induced by individuals sending an email exactly at time $t$ is likely to have limited interest.}, making the analysis irrelevant.

Many works propose to model interactions over time. Time-Varying Graphs are model-oriented: they aim at providing a model to unify these different approaches. Others, like temporal networks, study the interactions with a data-oriented point of view: they aim at answering to precise questions on datasets. Those approaches bring much progress on both fundamental and applied aspects, and we briefly review the main works in the field in Chapter~\ref{cha:ls:context}. Progress in the area however remains limited and there is still no consensus on an appropriate approach to handle interactions over time.

Our goal in this part of the thesis is language-oriented: we aim at defining a language to deal directly with link streams, in a way similar to what graph theory does for networks, or to what signal processing does for time series; we present this framework in Chapters~\ref{cha:ls:basics} to~\ref{cha:ls:cliques}. A key goal is to make our framework as simple and intuitive  as possible. We also meant it to be an extension of graph theory and signal processing for the study of interactions: a link stream that has no dynamics~\sidenote{\ie{} where all links last all the time.} should be exactly equivalent to a graph. Similarly, a link stream that has no structure~\sidenote{\ie{} where there are only two nodes.} should be exactly equivalent to a time series.

Finally, it is of importance that we cover many case studies and incorporate the existing state of the art as much as possible.

% We finally conclude and offer perspectives of our exploratory work in chapter~\ref{cha:conclu}.


% Structure globale : notions sur les graphes (signal), introduction des notions sur les flots, exemples pour former l'intuition

% We expect our framework to have the following desirable features:
% \begin{itemize}
% \item simple, intuitive, combinatorial and probabilistic expressions
% \item generalization of graphs and signals (the same on specific link streams, and relations between properties)
% \item if a notion is defined from another one, then the same
% \item cover many case studies, incorporate state-of-the-art on TVG etc
% \end{itemize}


%%%%
% A classical way to study link streams consists in studying the properties of graphs $G_{t..t+\Delta}$ for a given $\Delta$ and for $t = \alpha,\ \alpha+\Delta,\ \alpha+2\Delta,\ \dots$. In other words, one splits the link stream into consecutive slices of duration $\Delta$ and then studies the sequence of graphs obtained for each time slice \cite{}. This leads to studies of how graph properties evolve with time.
% 
% Another classical approach consists in studying the sequence of graphs $G_{\alpha..t}$ for $t = \alpha+\Delta,\ \alpha+2\Delta,\ \dots$ for a given $\Delta$ \cite{}. Such studies explore how graph properties evolve when the observation duration grows.

% Going further, one may consider values of $\Delta$ that evolve with time, and/or overlapping time slices \cite{}. In all cases, this makes it possible to use classical graph notions for studying link streams, but the choice of appropriate $\Delta$s is a challenge in itself and transforming link streams into graphs induces important losses of information. The goal of this paper precisely is to define a framework able to avoid this, see Section~\ref{sec:related}.


\chapter{Context}
\label{cha:ls:context}

% Sequences of interactions have been widely studied, including graph theory. Dynamics have been incorporated through dynamic graphs, ...

The goal of this chapter is to briefly review the main works modelling and studying interactions over time. We first quickly review methods with loss of information, and then methods that induce no loss of information.

\section{Models inducing information loss}

The simplest and most appealing way to model a sequence of interactions in order to study their structure is to resort to a graph where the nodes are the entities, and one puts a link between two nodes if they have interacted together, regardless of the number of interactions. For example,~\cite{strogatz2001exploring} explores the different ways to model complex networks of dynamical entities.

However, we focus in this short review of the literature on the ways of extending graph theory to handle interactions over time, and will not develop the rich body of work devoted to the study of interactions as static complex networks.

% Newman and co ?

% \cite{boccaletti2006complex}
% \cite{strogatz2001exploring}

% \cite{benson2016higher}
% \cite{}
% \cite{}
% \cite{}
% \cite{}


% \subsection{Time series and spectral analysis}

% A signal provides an coarse view of the network, by focusing on the evolution over time of one of its characteristics.

% Time series : parler de spectral clustering ? C'est une notion signal apportée aux graphes...

% ...

% % 3D tensors

% \cite{caceres2013temporal}
% \cite{hamon2016extraction}
% \cite{lacasa2008time}
% \cite{yang2008complex}
% \cite{kramer2009network}
% % \cite{transfo graph --> ts}
% \cite{}
% \cite{}
% \cite{}

% \subsection{Dynamic graphs and graph snapshots}

Given the descriptive power of graph theory, it is tempting to model sequences of interactions as sequences of graphs over time. Much work has been done in this direction, and we review  the main works in this section.

It is common to study the graph $G_{t..t+\Delta}$ from time $\alpha$, for a given $\Delta$ and for $t = \alpha,\ \alpha+\Delta,\ \alpha+2\Delta$, etc. One then ends up with a sequence of graphs $\{G_i\}_{i=1..k}$, also called snapshots depending on the context. Each element $G_i$ contains all interactions that happened between time $\alpha + i$ and time $\alpha + i + \Delta$. In those cases, one is typically interested in the statistics on a given snapshot $G_i$, or in comparing snapshots $G_i$ and $G_{i+1}$. See Figure~\ref{fig:seq-graphs} for an illustration.

\begin{figure}
	\includegraphics[width=1\linewidth]{graph-seq}
	\caption{A sequence of graphs $\{G_{t..t+\Delta}\}_{t=0,\ 5,\ 10,\ 15}$ for $\Delta=5$. The first graph of the sequence is the graph of all interactions that happened between time $0$ and time $5$; the second graph of the sequence is the graph of all interactions between time $5$ and time $10$, and so on.}
	\label{fig:seq-graphs}
\end{figure}

Other works study the graph $G_{\alpha..t}$ at a given starting time $\alpha$, for $t = \alpha+\Delta,\ \alpha+2\Delta,\ \dots$ for a given $\Delta$. In other words, it is the aggregated graph of all interactions that 
happened over time. See Figure~\ref{fig:aggr-graph} for an example.

\begin{figure}
	\includegraphics[width=1\linewidth]{aggr-graph}
	\caption{Study of the aggregated graph $G_{0..t}$ for $t = 5,\ 10,\ 15,\ 20$. The first graph of the sequence, $G_{0..5}$, contains all interactions between time $0$ and time $5$, the second graph of the sequence, $G_{0..10}$ contains all interactions between time $0$ and time $10$, and so on.}
	\label{fig:aggr-graph}
\end{figure}

Notice that since a graph can be represented by an adjacency matrix, one can also study a sequence of matrices $\{A(t)\}_{t}$, and study this sequence. This approach, called 3D tensors, is used by~\cite{gauvin2014detecting} to track groups of nodes over time.

Sequences of graphs have been much studied:~\cite{tang2010small} extend concepts of distance and paths in sequences of graphs, and end with the conclusion that the small-world effect known for graphs also applies in time to sequences of graphs. Before them,~\cite{leskovec2005graphs} had studied the densification and shrinking over time in sequences of graphs.

% Choix du $\Delta$, difficile.
Obviously, a key problem with these approaches is that one must choose appropriate values for $\Delta$: too small ones lead to trivial snapshots, while too large ones lead to important losses of information about the dynamics. As a consequence, much work has been done to design methods for choosing and assessing choices in the value of $\Delta$.

% + de refs sur le choix du Delta
~\cite{sulo2010meaningful} observe the evolution with $\Delta$ of common graph statistics such as the diameter or the density, and use time series tools to identify relevant time scales.~\cite{benamara2010estimating} produce a general methodology to assess whether a time window is large enough to characterize a given property.

In the context of mining high speed data streams,~\cite{hulten2001mining} point out the fact that the law governing the properties of data might change over time, especially over long durations. They propose a method for choosing a time window at a size where it is possible to detect such changes for given properties.

Recent work by~\cite{leo2015non} study the impact of a given time window on the resulting sequence of graphs, and show the existence of a \emph{saturation scale}, a value of $\Delta$ such that for all $\Delta' < \Delta$, the dynamics are mostly preserved, and for all $\Delta'' > \Delta$, the properties are altered. They design an automatic method to find the saturation scale in real-world datasets.

However, in all cases, the authors merge all interactions occurring in the same slice. ~\cite{lee2011multiscale} instead consider that there is not one scale that is adapted to study a sequence of graphs, but many. They devise a framework to study a sequence of graphs at many different scales simultaneously.

Independently from the choice of appropriate values of $\Delta$, the sequence of graphs itself is a complex object that is hard to manipulate.

% Unfortunately, these models become increasingly complex as the number of scales grows. 

% \cite{bilgin2006dynamic}
% \cite{aynaud2011multi}

\section{Models inducing no information loss}
\label{sec:ls:context:no-loss}

We now turn our attention to the transformations and models of sequences of interaction that induce no loss of information.

	\subsection{Static graphs from sequences of interactions}

In order to benefit from the tools of graph theory when studying interaction sequences, one can build a graph where the edges are labeled with their time of existence. The first work on the subject appears to be by~\cite{berman1996vulnerability}, and~\cite{kempe2000connectivity} are responsible for coining the term \emph{temporal networks}. However, while this representation induces no information loss, the object itself is complicated to manipulate, and extending definitions to such labelled graphs is hard.

Instead of resorting to sequences of graphs,~\cite{kostakos2009temporal} propose to create a graph where each entity at each time is a node, and one puts a link between two nodes if they are linked in the graph, or is they are contiguous in time. See Figure~\ref{fig:temporal-graph} for an illustration. The links in the graph are of two natures: some of them are temporal links, and some of them are structural links. This approach, however, assumes that structural and temporal links carry similar meaning.

There are other variants, such as the one by~\cite{queyroi2014delta}, where having two nodes $(u,t)$ and $(u,t')$, $t'>t$, means that $t'-t \geq \Delta$, for a given $\Delta$.

% ~\cite{mitra2012intrinsically} build a time-directed graph from a citation dataset and ...

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{temporal-graph}
	\caption{A temporal graph $G=(V,E)$, with $V=\{(u,1),(u,2),(v,3),\dots\}$, and
	$E=\{((u,1),(u,2)), ((u,1),(v,1)),\dots\})$. $((u,1),(u,2))$ is a \emph{temporal} edge, and $((u,4),(x,4))$ is a \emph{structural} edge. An interaction in this graph is a link at time $i$ between two nodes $(u,i)$ and $(v,i)$.}
	\label{fig:temporal-graph}
\end{figure}

	\subsection{Temporal networks}

A \emph{temporal network} is simply a sequence of interactions. Originally defined by~\cite{holme2012temporal}, a variety of works fall under this denomination, and we summarize the main ones below. See Figure~\ref{fig:temporal-network} for an example.

The definition itself of a temporal network is not formalized~\cite{holme2012temporal}, and is simply a sequence of interactions. A consequence of this is the multiplication of works under other names, such as time varying networks, etc.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{temporal-network}
	\caption{Representation of a temporal network, from~\cite{holme2012temporal}. Nodes (A,B,C,D) are represented horizontally with their names on the left of the diagram, and a link between two nodes $A$ and $B$ at abscissa $1$ means that $A$ and $B$ interacted together at time $1$.}
	\label{fig:temporal-network}
\end{figure}

The community of researchers working on temporal networks has mainly focused on questions related to paths in temporal networks: diffusion, reachability, and so on. 

Paths in temporal networks have been extensively studied by~\cite{pan2011path}. The authors define different notions of paths that have no counterpart in graphs, and study the correlations between temporal paths and paths in the static graph induced by the temporal network; finally, they define an extension of closeness centrality to temporal networks.

Working upon these definitions,~\cite{nicosia2013graph} proposes extensions of connectivity, closeness, betweenness, and spectral centrality, as well as studying temporal motifs. 

Random walks are extended to temporal networks by~\cite{starnini2012random}; the same authors present different randomizing strategies, allowing one to single out the role of different temporal properties on empirical networks. More recently,~\cite{saramaki2015exploring} investigate the correlations between link activations in temporal networks with greedy walks.

Diffusion has been thoroughly studied on temporal networks.~\cite{redmond2016subgraph} formulates the problem of identifying sequences of link activations that lead to the spread of a disease in terms of a time-respecting subgraph isomorphism problem.

Identifying the epidemic threshold~\sidenote{According to the World Health Organization, the epidemic threshold is "the critical number or density of susceptible hosts required for an epidemic to occur".} has attracted a lot of attention.~\cite{valdano2015analytical} analytically compute the epidemic threshold on temporal networks, and recent work by~\cite{vestergaard2016impact} studies the minimum number of sensors that should be distributed in a population of $n$ people to minimize the epidemic risk. Recently,~\cite{holme2016temporal} released a preprint studying disease spreading on $8$ datasets of human contacts.

~\cite{takaguchi2012importance} focuses on the identification of interactions in time that ease diffusion, and that are of importance to other nodes than just the two nodes involved by an interaction. They assess the importance of such interactions in temporal networks, and come to the conclusion that the diffusion of information is eased in real-world temporal networks by the bursty nature of their interactions.

Temporal networks had no formal basis until the recent work of~\cite{batagelj2016algebraic}. The authors provide algebraic definitions of temporal networks. The authors define notions of neighborhood, degree, and spectral centralities as algebraic operations on a semiring. 

A recent review of the main works in the field of temporal networks has been published by~\cite{holme2015modern}.

	\subsection{Time-Varying Graphs}

% Formal ...

% Let us take the example of ... . ... . This sequence of interactions can be reprensented without loss of information by both link streams and time-varying graphs: in that sense, the two models are equivalent. However, the reprensentations for both these models illustrate well the perspective shift induced by link streams. In figure~\ref{fig:tvg}, nodes a ... 

Time-Varying Graphs model evolving graphs, graphs where links and nodes can appear and disappear at arbitrary instances of time, see Figure~\ref{fig:tvg} for an example.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{tvg-casteigts}
	\caption{A Time-Varying Graph. Each link of the graph is labelled with the intervals where it is available: $[x,y)$ means that edge $(u,v)$ is available from time $x$ (included) to time $y$ (excluded). For instance, the link $(a,b)$ is available from time $1$ to time $5$; the link $(c,d)$ is available from time $2$ to time $4$, and then from time $6$ to time $8$.}
	\label{fig:tvg}
\end{figure}

~\cite{casteigts2012time} associate functions of presence $\rho(u,v)$ to edges: $\rho \mapsto \{0,1\}$ indicates whether a given link is available at a given time $t$, which corresponds to labelling links with their presence times. The authors then formally define concepts of subgraphs, of journeys~\sidenote{A journey is a time-respecting path.}, and finally define a hierarchy of classes of Time-Varying Graphs based on their temporal properties. In~\cite{casteigts2015expressivity}, the authors define the language of feasible journeys given waiting time constraints, and study the expressivity of these languages.

~\cite{santoro2011time} study the evolution of temporal and atemporal~\sidenote{Defined on a Time-Varying Graph versus defined on a sequence of static graphs.} statistics over time. This is interesting to study the convergence in time of statistics, for instance in a distributed system.

More recently,~\cite{braud2016next} has published proofs of impossibility results, to prove for instance the convergence of deterministic algorithms on Time-Varying Graphs.

% \begin{figure}
% 	\centering
% 	\includegraphics[width=0.7\linewidth]{tvg-casteigts}
% 	\caption{title.}
% 	\label{fig:tvg-casteigts}
% \end{figure}

~\cite{wehmuth2015unifying} aim at proposing a unique framework to unify evolving graphs representations. They rely on a reduction of the Multi-Aspect Graph model introduced by~\cite{wehmuth2015multiaspect}, which is a general model for representing multilayer graphs~\sidenote{In a multiplex graph, nodes are distributed in \emph{layers}, and links can exist between two nodes of the same layer or two nodes of different layers.} varying over time.

\section{Conclusion}

We briefly presented the existing works aiming at describing interactions over time. We have structured this presentation in two parts: on the one hand, representations that induce a loss of information, and on the other hand, representations that do not induce such loss.

Since no information is lost for the models presented in Section~\ref{sec:ls:context:no-loss}, these models are equivalent for representing a given data: given a representation of a finite sequence of interactions as a Time-Varying Graph, for instance, one can readily define the associated temporal graph, or temporal network.

However, the different representations of each of these models is a good illustration of the fact that they correspond to different perspectives of the same underlying object: while in (evolving) graphs, focus is set on the graph structure at each instant, in the case of temporal networks, nodes are represented as horizontal lines, and focus is set on the activations of links through time.

In the link stream framework, we set focus on the fact that links arrive as a stream, like in temporal networks; however, unlike temporal networks, our goal is to define a comprehensive and consistent language to describe sequences of interactions.

% \subsection{Other models}

% \cite{bramsen2006inducing} <-- C'est cool mais c'est pour ordonner temporellement les événements d'un texte. 
% \cite{han2014chronos}
% \cite{}
% \cite{}
% \cite{}
% \cite{}
% \cite{}