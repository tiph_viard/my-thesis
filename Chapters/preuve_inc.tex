\begin{lemma}[]\label{lemma:clique_transfo}
In Algorithm~\ref{alg:dcliques}, all elements of S are \dclique s of $L$.
\label{lemma1}
\end{lemma}

\begin{proof}

We prove the claim by induction on the iterations of the {\em while} loop (Lines~\ref{alg:begin_loop} to~\ref{alg:end_loop}).

Initially, all elements of $S$ are \dclique s (Line~\ref{alg:init_state}).
Let us assume that all the elements of $S$ are \dclique s at the $i$-th iteration of the loop (induction hypothesis). The loop may add new elements to $S$ at Lines~\ref{alg:add_clique_node}, \ref{alg:add_clique_time_b} and~\ref{alg:add_clique_time_e}.
In all cases, the added element is built from an element $C=\clique{X}{b}{e}$ of $S$ (Line~\ref{alg:get_clique}), which is a \dclique\ by induction hypothesis.

It is trivial (from the test at Line~\ref{alg:check_clique_node}) that Line~\ref{alg:add_clique_node} only adds \dclique s.

Let us show that $(X,[b,l+\Delta])$, 
where $l$ is computed in Line~\ref{alg:get_l}, necessarily is a \dclique. 
As $\clique{X}{b}{e}$ is a \dclique\ all links in $X\times X$ appear at least once every $\Delta$ from $b$ to $l\le e$.
Moreover, since $l$ is the earliest last occurrence time of a link in $C$,
for all $u$ and $v$ in $X$ there is necessarily a link $(t,u,v)$ in $E$ with $l\le t\le e$.
Notice also that $l \ge e-\Delta$, otherwise $\clique{X}{b}{e}$ would not be a \dclique. 
Therefore a link between $u$ and $v$ occurs at least once between 
%$l\le e$ and $l+\Delta\ge e$ 
$l$ and $l+\Delta$
for all $u$ and $v$ in $X$.
Finally, $\clique{X}{b}{l+\Delta}$ is a \dclique{}.
% and since $e<e'\le l+\Delta$, the element $(X,[b,e'])$ added to $S$ at Line~\ref{alg:add_clique_time_e}, is a \dclique.

The same arguments hold for Line~\ref{alg:get_f}.

Finally, at the end of the $(i+1)$-th iteration of the loop, all the elements of $S$ are \dclique s, which ends the proof.
\end{proof}

\begin{lemma}\label{lemma:maximal}
All the elements of the set returned by Algorithm~\ref{alg:dcliques} are maximal \dclique s of $L$.
\end{lemma}

\begin{proof}

Let $C=\clique{X}{b}{e}$ be an element of $R$ returned by the algorithm.
Only elements of $S$ are added to $R$ (at Line~\ref{alg:add_c_r}), and so according to Lemma~\ref{lemma1} $C$ is a \dclique.
Assume it is not maximal; then we are in one of the three following situations.

There exists $v$ in $V\setminus X$ such that $\clique{X\cup \{v\}}{b}{e}$ is a \dclique. Then $v$ is found at Lines~\ref{alg:add_node_begin}\textendash\ref{alg:check_clique_node},
and Line~\ref{alg:nodeismaxfalse} sets the boolean {\em isMax} to {\em false}. Therefore, Line~\ref{alg:test_max} ensures that $C=\clique{X}{b}{e}$ is not added to $R$, and we reach a contradiction.

There exists $e'>e$ such that $\clique{X}{b}{e'}$ is a \dclique\ and we assume without loss of generality that there is no link between nodes in $X$ from $e$ to $e'$.
Then, let us consider  $l \in [b,e]$, computed in Line~\ref{alg:get_l}, which
is the earliest last occurrence time of a link in $C$.
We necessarily have $l \ge e'-\Delta$ because  $\clique{X}{b}{e'}$ is a \dclique.
Since  $e'>e$, this implies
 $l > e-\Delta$.
As a consequence, the test in Line~\ref{alg:check_e_l} of the algorithm is satisfied, and Line~\ref{alg:eismaxfalse} sets the boolean {\em isMax} to {\em false}. Like above, we reach a contradiction.

If there exists $b'<b$ such that $\clique{X}{b'}{e}$ is a \dclique, then similarly to the previous case we reach a contradiction.

Finally, $C$ necessarily is maximal, which proves the claim.
\end{proof}

\newcommand{\ff}{{\ensuremath{s}}}

Before proving our main result, which is that all maximal \dclique{}s are returned
by the algorithm,
we need the following two intermediate results.

\begin{lemma}
  \label{lem:width}
  Let $C = \clique{X}{b}{e}$ be a maximal \dclique{} of $L$,
  and let $\ff$ be the earliest occurrence time of a link in $C$.
  Then $e \ge \ff{} + \Delta$.
\end{lemma}

\begin{proof}
  Since $C$ is a \dclique{} and by definition of \ff{}, for all $u, v$ in $X$ there exists at least one link $(t,u,v)$ such that $\ff{} \le t\le e$.  Assume $e < \ff + \Delta$; then for all $u,v$ in $X$ there also exists a link $(t,u,v)$ such that $\ff{} \le t \le e < \ff + \Delta$.  Therefore $\clique{X}{b}{\ff+\Delta}$ is a \dclique{} and $C$ is included in it, which means that $C$ is not maximal and we reach a contradiction.
\end{proof}

\begin{lemma}
\label{lem:path}
Let $C = \clique{X}{b}{e}$ be a maximal \dclique{} of $L$ and let $\ff$ be the earliest occurrence time of a link in $C$.
If 
%$e \ge \ff + \Delta$ and 
$\clique{X}{\ff}{\ff+\Delta}$ is in $S$ at some stage of Algorithm~\ref{alg:dcliques}, then $C$ is in the set returned by the algorithm.
\end{lemma}

\begin{proof}
Assume $C_0 = \clique{X}{\ff}{\ff+\Delta}$ is in $S$ and consider the longest sequence of steps of Algorithm~\ref{alg:dcliques} of the form:
$C_0 \rightarrow C_1 \rightarrow \cdots \rightarrow C_k$ such that for all $i$ $C_i = \clique{X}{\ff}{e_i}$ with $e_{i+1}>e_i$. In other words, the algorithm builds $C_{i+1}$ from $C_i$ in Lines~\ref{alg:get_l} to~\ref{alg:add_clique_time_e}
(notice that $e\ge s+\Delta$ from Lemma~\ref{lem:width} and so $C_0$ is included in $C$).

We prove that $C_k = \clique{X}{\ff}{e}$ by contradiction. Assume this is false, and so that $e_k \not= e$. 
As $C$ is maximal, we then necessarily have $e_k < e$.
In addition, $e_k = l+\Delta$ where $l$ is the earliest last occurrence time of a link in $C_{k-1}$ computed at Line~\ref{alg:get_l}.
Since $C_k$ is the last \dclique{} in the sequence, 
$l$ is also the earliest last occurrence time of a link in $C_k$
(otherwise there would be a clique $C_{k+1}$ satisfying the constraints of the sequence above).
Therefore there exist $u,v\in X$ such that $(l,u,v)\in E$ and such that
there is no occurrence of a link $(u,v)$ between $l$ and $e_k = l+\Delta$.
This ensures that there exists an $\epsilon$ such that 
%$e_k < l + \Delta + \epsilon < e$
$l + \Delta + \epsilon < e$ and such that there is no link between $u$ and $v$ from $l+ \epsilon$ to $l + \Delta + \epsilon$, which contradicts the assumption that $C$ is a \dclique{}.

We now show that the algorithm builds $C$ from $C_k$ to end the proof. Since $C$ is maximal, there exists $u,v \in X$ such that $(b+\Delta, u,v) \in E$ and such that there is no other link between $u$ and $v$ from $b$ to $b+\Delta$. By definition of $\ff$, $b+\Delta \ge \ff$. Therefore the latest first occurrence time of a link in $C_k$, $f$, is equal to $b+\Delta$ and Lines~\ref{alg:get_f} to~\ref{alg:add_clique_time_b} build $C$ from $C_k$.
\end{proof}


\begin{lemma}\label{lemma:clique_decouverte}
All maximal \dclique s of $L$ are in the set returned by Algorithm~\ref{alg:dcliques}. 
\end{lemma}

\begin{proof}
It is easy to check that if $S$ contains a maximal \dclique\ then it is added to the set $R$ returned by the algorithm, and only these \dclique{}s are added to $R$.
We therefore show that all maximal \dclique s are in $S$ at some stage. 

Let $C = \clique{X}{b}{e}$ be a maximal \dclique{} of $L$, let $\ff$ be the earliest occurrence time of a link in $C$, and let $u,v\in X$ be two nodes such that there exists a link between them at $\ff$ (\ie{}, $(\ff, u,v) \in E$).
We show that there is a sequence of steps of the algorithm that builds $C$ from \dclique{} $C_0 = \clique{\{u,v\}}{\ff}{\ff}$ (which is placed in $S$ at the beginning of the algorithm, Line~\ref{alg:init_state}).

Lines~\ref{alg:get_l} to~\ref{alg:add_clique_time_e} builds $C_1 = \clique{\{u,v\}}{\ff}{\ff+\Delta}$ from $C_0$.

Notice that for all subset $Y$ of $X$, $\clique{Y}{\ff}{\ff+\Delta}$ is a \dclique{}. Therefore the algorithm iteratively adds all elements of $X$ at Lines~\ref{alg:add_node_begin} to~\ref{alg:add_node_end}, finally obtaining $C'=\clique{X}{\ff}{\ff+\Delta}$ from $C_1$.

%Since Lemma~\ref{lem:width} ensures that $e \ge \ff + \Delta$, 
We finally apply Lemma~\ref{lem:path} to conclude that the algorithm builds $C$ from $C'$.

% To do so,
%   first notice that each \dclique\ $A$ added to $S$ in the {\em while}
%   loop of our algorithm (Lines~\ref{alg:begin_loop} to
%  ~\ref{alg:end_loop}) is built from a \dclique\ $B$ taken from the set
%   $S$ at Line~\ref{alg:get_clique}. We denote this by $B \rightarrow
%   A$.

% Now, let us consider any maximal \dclique\ $C = \clique{Y}{x}{y}$,
% and let us denote by $x'\ge x$ (resp. $y' \le y$) the first (resp. last) occurrence time of a link involving an element of $Y$ after $x$ (resp. before $y$). Notice that this link is not necessarily in $C$, as the other involved node may be outside $Y$.
% We define $C'=\clique{Y}{x'}{y}$ and $C''=\clique{Y}{x'}{y'}$.

% If $x'=x$ then trivially $C'=C$. If $x\not= x'$ then we show that $C' \rightarrow C$.
% Assume $C'$ is the element taken from $S$ at Line~\ref{alg:get_clique}: $X=Y$, $b=x'>x$ and $e=y$.
% As $C$ is maximal, there exist $u$ and $v$ in $Y$ such that the first occurrence of $(u,v)$ in $C$ is $(x+\Delta,u,v)$
% (otherwise, there would be an $\epsilon$ such that $(Y,[x-\epsilon,y])$ would be a \dclique, and so $C$ would not be maximal).
% In addition, for all $u$ and $v$ in $Y$, 
% $x\le f_{xuv} \le x+\Delta$
% (otherwise $C$ would not be a \dclique). 
% Since there is no link involving nodes in $X=Y$ from $x$ to $x'=b$, 
% the value of $f$ computed in Line~\ref{alg:get_f} is $f=x+\Delta$.
% Since $x' \not= x$, we have $f = x+\Delta \not= x'+\Delta = b+\Delta$ and condition of Line~\ref{alg:check_b_f} holds. 
% Now, by definition of $x'$,
% there is no value of $t$, $x = f-\Delta \le t < b=x'$,
% such that a there is a link $(t,u,v)$ with $u$ or $v$ in $X=Y$.
% The condition in Line~\ref{alg:get_tf} is therefore not satisfied,
% and Line~\ref{alg:b_default} sets $b'$ to $f-\Delta = x$;
% the \dclique\ added to $S$ at Line~\ref{alg:add_clique_time_e} is nothing but $\clique{X}{b'}{e} = \clique{Y}{x}{y} = C$, hence $C'\rightarrow C$.

% Similarily, $C''=C'$ or $C'' \rightarrow C'$. Therefore, if $C''$ is in $S$ at some stage, then $C'$ and $C$ also will.

% In order to show that $C''$ is in $S$ at some stage,
% we build a sequence of \dclique s $C_n$, $C_{n-1}$, $\dots$, $C_0$
% such that $C_n = C''$, and $C_0 = \clique{\{u,v\}}{t}{t}$ with $(t,u,v)\in E$.
% We build this sequence in order to ensure that for all $i$, $C_i \rightarrow C_{i+1}$. As $C_0$ is in $S$ from Line~\ref{alg:init_state}, this ensures that $C''=C_n$ is finally added to $S$, and so $C$ also is, thus ending the proof.

% For the sake of the proof, we will in addition show that the following property $P_i$ holds for all $i$: $C_i = \clique{X_i}{b_i}{e_i}$ is a \dclique\ and there is a link involving an element of $X_i$ at time $b_i$ and at time $e_i$. Since $C_n=C''$, property $P_n$ is true from the construction above. We will show that, for all $i$, if it is true for $C_i$ then it is also true for $C_{i-1}$.

% Let us consider $C_i = \clique{X_i}{b_i}{e_i}$. We distinguish two main cases.

% If $b_i=e_i$ and $|X_i|=2$ then $C_i$ is of the form $\clique{\{x,y\}}{t}{t}$, and so $C_i = C_0$, which ends the proof.
% If $b_i=e_i$ and $|X_i|\not= 2$ then we define 
% $C_{i-1}$ as $ \clique{X_i\setminus \{x\}}{b_i}{e_i}$ for any $x$ in $X_i$;
% it is trivial to check that $C_{i-1} \rightarrow C_i$ from Lines~\ref{alg:add_node_begin} to~\ref{alg:add_node_end}, and that $P_{i-1}$ is true.


% If $b_i\not= e_i$, then we distinguish the following three sub-cases.

% \begin{itemize}

% \item
% If there exists $x$ and $y$ in $X_i$ such that $(b_i,x,y) \in E$
% and if $|X_i|>2$, 
% then we define $C_{i-1}$ as $\clique{X_i\setminus \{x\}}{b_i}{e_i}$ 
% if there is no $x'$ such that $(e_i,x,x') \in E$, 
% and as $\clique{X_i\setminus \{y\}}{b_i}{e_i}$ otherwise.

% \item
% If there exists $x$ and $y$ in $X_i$ such that $(b_i,x,y) \in E$ as above, 
% but now $|X_i|=2$, 
% we define $C_{i-1}$ as $\clique{X_i}{b_i}{z}$ 
% where $z$ is the largest $z<e_i$ such that there is a link $(z,u,v) \in E$
% with $u$ or $v$ in $X_i$.

% \item
% Otherwise (there is no link $(b_i,x,y)$ with $x$ and $y$ in $X_i$), 
% %then from $P_i$ there exists $x$ in $X_i$ and $y$ not in $X_i$ such that $(b_i,x,y) \in E$
%  then we define $C_{i-1}$ as $\clique{X_i}{a}{e_i}$ 
% where $a$ is the smallest $a>b_i$ such that there is a link $(a,x,y) \in E$ 
% with $x$ or $y$ in $X_i$.
% \end{itemize}

% We will show for each case that $C_{i-1} \rightarrow C_i$ and that $P_{i-1}$ is true. To do so, let us assume that $C_{i-1}$ is the element taken from $S$ at Line~\ref{alg:get_clique} of the algorithm: $X = X_{i-1}$, $b=b_{i-1}$ and $e=e_{i-1}$.

% In the first case, it is trivial to check that $C_{i-1} \rightarrow C_i$ from Lines~\ref{alg:add_node_begin} to~\ref{alg:add_node_end}. 
% Let us show that $P_{i-1}$ is true. 
% Clearly, $C_{i-1}$ is a \dclique. Since only one of the nodes $x$ or $y$ is removed, the link $(b_i=b_{i-1}, x, y)$ 
% is such that
% $x$ or $y$ is in $X_{i-1}$.
% %Clearly, $C_{i-1}$ is a \dclique. In addition, 
% If there is no $x'$ such that $(e_i,x,x') \in E$ then 
% because of $P_i$ there exists some link 
% $(e_i, u,v)$ such that $u$ or $v$ is in $X_i \backslash \{x\}=X_{i-1}$, 
% and therefore $P_{i-1}$ holds.
% % thanks to $(e_i,x',y')$ because $x\not= x'$, $x'\not= y'$ and $x\not= y'$ and so $x'$ or $y'$ is in $X_{i-1} = X_i\setminus \{x\}$.
% %from $P_i$ there is a link $(e_i,x',y')$ in $L$ with $x'$ or $y'$ in $X_i$. 
%  If such a $x'$ exists, then the link $(e_i=e_{i-1},x,x')$ involved a node of $X_{i-1}$ and
% $P_{i-1}$ also holds.

% In the second case, $P_{i-1}$ trivially holds and we denote by $x$ and $y$ the two elements of $X_i$.
% We have $X = X_{i-1} = X_i = \{x,y\}$, $b = b_{i-1} = b_i$, and $e=e_{i-1}=z$. 
% The value of $l$ computed in Line~\ref{alg:get_l} is the last occurrence time of $(x,y)$
% before $e = e_{i-1} = z$.

% We first show that the condition in Line~\ref{alg:check_e_l} is satisfied, 
% {\em i.e.} $e \not= l + \Delta$. 
% Let us show that
% there exists a $t$, $e_i - \Delta \le t < e_i$, such that $(t,x,y) \in E$: 
% if $e_i-b_i \le \Delta$ then $t=b_i$ satisfies this property; 
% if $e_i-b_i > \Delta$, then if the property is not satisfied then there is an $\epsilon$ such that the interval $[e_i-\epsilon,e_i-\Delta-\epsilon]$ contains no occurrence of a link between $x$ and $y$, which contradicts the fact that $C_i$ is a \dclique. 
% Moreover, by definition of $z$ there is no link involving $x$ or $y$ 
% from $z=e_{i-1}=e$ to $e_i$,
%  and so there exists a $t$, $e_i - \Delta \le t \le e_{i-1}$, such that $(t,x,y) \in E$.
% Finally,
% $l$ is the last occurrence time of $(x,y)$ before $e_{i-1}$ and therefore we have
% $e_{i}-\Delta \le l \le e_{i-1}$. 
% As $z<e_i$, we have $l > z-\Delta$  and therefore $l\not= e-\Delta$.

% We now show that the condition of Line~\ref{alg:get_tl} also is satisfied. From $P_i$, there exists a link $(e_i,u,v)$ such that $u$ or $v$ is in $X$. 
% Let us take $t=e_i$. 
% We then have $e = e_{i-1} = z < e_i = t$ and we have shown  above that $l\ge e_i - \Delta=t-\Delta$, 
% therefore condition of Line~\ref{alg:get_tl} is satisfied.

% In addition,
% by definition of $z$  
% there is no occurrence of a link involving $x$ or $y$ strictly before $e_i$ and after $e = e_{i-1} = z$.
%  Therefore, Line~\ref{alg:set_e} sets $e' = t = e_i$, 
% and the \dclique\ added at Line~\ref{alg:add_clique_time_e} is $\clique{X}{b}{e'} = \clique{X_i}{b_i}{e_i} = C_i$, hence $C_{i-1} \rightarrow C_i$.


% In the third case, it is also trivial that $P_{i-1}$ holds (just notice that $C_{i-1}$ necessarily is a \dclique\ as there is no link between elements of $X_i = X_{i-1}$ at $b_i$ and between $b_i$ and $b_{i-1}$). We have $X = X_{i-1} = X_i$, $b = b_{i-1} = a > b_i$, and $e=e_{i-1}=e_i$.

% We first show that the condition in Line~\ref{alg:check_b_f} is satisfied. 
% As $C_i$ is a \dclique, and as there is no link between two nodes in $X_i$ at $b_i$, 
% then there must exist a link for all pairs of nodes in $X_i$ at a time $t$ in $]b_i,\min(b_i+\Delta,e_i)]$. 
% There is no link involving nodes of $X_i$ before $a$, by definition of $a$, and so $t$ necessarily is in $[a,\min(b_i+\Delta,e_i)]$. 
% The value of $f$ computed in Line~\ref{alg:get_f} is the
% latest first occurrence time after $b=b_{i-1}=a$ of a link 
% between two nodes of $X=X_{i-1}=X_i$.
% Therefore, $f$ also is in $[a,\min(b_i+\Delta,e_i)]$. 
% Therefore, $f \le b_i+\Delta < a+\Delta = b+\Delta$ and so $f-\Delta < b$.

% In addition, similarly to condition of Line~\ref{alg:get_tl} in the previous case, condition of Line~\ref{alg:get_tf} is satisfied when $t=b_i$. This ultimately leads to $C_{i-1} \rightarrow C_i$, which ends the proof.

\end{proof}

From these lemmas, we finally obtain the following result.

\begin{theorem}
Given a link stream $L$ and a duration $\Delta$, Algorithm~\ref{alg:dcliques} computes the set of all maximal \dclique s of $L$.
\end{theorem}
