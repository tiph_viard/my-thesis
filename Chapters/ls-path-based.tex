\chapter{Paths-based notions}
\label{cha:ls:paths}

% \noteperso{un foremost particulier: on saute des qu'on peut (flooding, earliest); calcul des shortest foremosts (et donc des shortests) en deux passes: flooding en avant puis en arriere}

% \noteperso{chemin elementaire ? passant une seule fois par un noeud, par un instant ? cycles ? greedy path}

% \noteperso{see Holme "Temporal Networks" and others: paths, latency, centralities, trees, etc}

In many contexts, graphs are used to study diffusion between nodes: delivering a message, or a file; assessing the resilience of a network to cable outages;predicting the spreading of a disease; etc. In all those cases, the underlying concept is the one of \emph{path}, which is fundamental for defining notions of reachability and connectivity.

In many contexts, one is interested in assessing the importance of individual nodes or links: to identify key actors in social networks, or to target the weakest points of a network. Paths also lead to notions of centrality designed to capture such behaviour.

We discuss in this chapter these notions, and extend them to link streams.

\section{Paths}

In a simple graph $G=(V,E)$, a path $P$ from $u \in V$ to $v \in V$ is a sequence $(u_0,v_0)$, $(u_1,v_1)$, $\dots$, $(u_k,v_k)$ such that $u_0 = u$, $v_k = v$, and for all $i$, $v_{i-1}=u_i$ and $(u_i,v_i)$ is in $E$. The integer $k$ is the length of $P$. See Figure~\ref{fig:paths-graph} for an illustration.

\begin{marginfigure}
	\centering
	\includegraphics[width=70px]{paths-graph}
	\caption{A graph $G=(\{u,x,y,\dots\}, \{(u,x), (s,y),\dots\})$ and a path $P$ of length $4$ from node $u$ to node $v$; $P=((u,x),(x,y),(y,z),(z,v))$.}
	\label{fig:paths-graph}
\end{marginfigure}

In a simple link stream $L = (T,V,E)$, a $\gamma$-path $P$ from node $u \in V$ to node $v \in V$ is a sequence $l_0=(b_0,e_0,u_0,v_0)$, $l_1=(b_1,e_1,u_1,v_1)$, $\dots$, $l_k=(b_k,e_k,u_k,v_k)$ such that $u_0 = u$, $v_k = v$ and for all $i$, $l_i$ is a sub-link of a link in $E$, $u_i = v_{i-1}$, $b_i \ge e_{i-1}$, and $e_i = b_i + \gamma$~\sidenote{Notice that $(t,t,u,v)$ is a valid sub-link, if $\gamma=0$.}. We call $\gamma$ the delay of links. For all $i$, we say that $P$ involves $u_i$ and $v_i$, and that it involves them at all $t$ in $[b_i,e_i]$. See Figures~\ref{fig:paths} and~\ref{fig:gamma-paths} for examples.

We say that path $P$ starts at $t_0$, arrives at $t_k$, has duration $t_k-t_0$ and length $k$. Given two nodes $u$ and $v$, we say that a path from $u$ to $v$ of minimal length is a shortest path, that a path with minimal duration is a fastest path, and a path with minimal arrival time is a foremost path~\sidenote{These properties can be cumulative: the shortest fastest path is, among all fastest paths, the shortest one; the fastest foremost is, among all foremost paths, the fastest one, and so on.}. The length $\distance(u,v)$ of a shortest path from $u$ to $v$ is the distance from $u$ to $v$, and the duration $\latency(u,v)$ of a fastest path is the latency from $u$ to $v$. We denote by $\timetoreach_t(u,v)$ the time needed to reach $v$ from $u$ at time $t$, \ie\ the difference between $t$ and the minimal arrival time of a path from $u$ to $v$ starting after $t$. By extension, we denote by $\timetoreach_{b..e}(u,v)$ the time needed to reach $v$ from $u$ at time $t$ with a path starting between $b$ and $e$. In the remainder of this chapter, $\gamma=0$ unless stated otherwise.

Notice that these notions have previously been defined by~\cite{xuan2003computing}, and we simply formulate them in the link stream framework.

\begin{figure}
\centering
	\includegraphics{paths-ls}
	\caption{A link stream and $3$ $0$-paths (\ie{} $\gamma=0$) from $a$ to $d$ from time $t$. Path $((1,3,a,b),(4,5,b,c),(5,5,c,d))$ (in red) arrives at time $5$, has duration $ $ and length $3$; it is a \emph{foremost} path. Path $((7,8,a,b),(8,9,b,c),(9,9,c,d))$ (in blue) arrives at time $9$ has duration $2$ and length $3$; it is a \emph{fastest} path. Path $((15,17,a,c),(18,18,c,d))$ (in green) arrives at time $16$, has duration $3$ and length $2$; it is a \emph{shortest} path.}
	\label{fig:paths}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.8\linewidth]{path_duration}
	\caption{A link stream and a $\gamma$-path from $a$ to $b$, with $\gamma=1$ unit of time.}
	\label{fig:gamma-paths}
\end{figure}

If $\gamma = 0$ then $b_i=e_i$ in the definition above, and a $0$-path is equivalent to a sequence of triplets $(t_0,u_0,v_0)$, $(t_1,u_1,v_1)$, $\dots$, $(t_k,u_k,v_k)$ such that $u_0 = u$, $v_k = v$, and for all $i \in [1,k]$, $v_{i-1}=u_i$, $t_i \ge t_{i-1}$ and there is a link $(b_i,e_i,u_i,v_i)$ in $E$ such that $t_i \in [b_i,e_i]$.
% Notice that $\gamma$-paths in $L=(T,V,E)$ are {\em not} equivalent to $0$-paths in $L'=(T,V,E')$ where $E' = \{ (b',e,u,v): (b,e,u,v)\in E, e\ge b+\gamma, b'=b+\gamma\}$. See figure~\ref{fig:gamma-path-stream}.


% \begin{marginfigure}
% 	\centering
% 	\includegraphics{gamma-path-stream}
% 	\caption{$\gamma$-paths in $L$ are not equivalent to $0$-path in a stream $L'$ where all links start $\gamma$ later. The $\gamma$-path in $L$ is represented in a dotted line, and the $0$-path in $L'$ is in red.}
% 	\label{fig:gamma-path-stream}
% \end{marginfigure}

Several variants of this notion of path in link streams make sense. For instance, one may capture the fact that each node cannot forward information immediately after receiving it by adding the constraint $t_{i+1} \ge t_i + \gamma'$ in the definition, for a given $\gamma'$. Notice that this is {\em not} equivalent to $\gamma+\gamma'$-paths. One may also impose an overlap between successive links involved in a path: conditions above then become $b_{i+1} \leq e_i$.

Similarly, one may want to impose non-null delays on links and/or nodes but without bounds on these delays. Conditions above then become $e_i>b_i$ and $t_{i+1}>t_i$, respectively.

% \noteperso{Finally, one may impose an overlap between successive links involved in a path, leading to the additional condition... need of the b and e of links; exists in previous works??}

% \begin{paragraph}{$\Delta$-analysis}
% Given a $\Delta$, we define a $\gamma$-path in an instantaneous link stream $L$ as a sequence $(t_i,u_i,v_i)$ such that for all $i$, for all $j \le i$, $t_i \ge t_j-\Delta$. In other words, it is possible to go backwards in time of $\Delta$, but never more. These paths are equivalent to $\gamma$-paths in $L_\Delta$. Indeed, let $(t_i,u_i,v_i)$ be such a path. Then, for all $i$, there exists $(s_i,u_i,v_i)$ in $L$ such that $t_i \in [s_i,s_i+\Delta]$. We show that $(s_i,u_i,v_i)$ is a path in $L$, {\em i.e.} that for all $i$ and $j\le i$ we have $s_i \ge s_j-\Delta$ by contradiction: let us assume that there exists $i$ and $j \le i$ such that $s_i < s_j - \Delta$. First notice that by definition of $0$-paths in $L_\Delta$, we have $t_i \ge t_j$. In addition, $t_j \ge s_j > s_i + \Delta$ by definition of $s_j$ and because of our contradiction hypothesis. As a consequence, $t_i > s_i + \Delta$, which contradicts the definition of $t_i$.
% \end{paragraph}

\section{Connectivity}

In a graph $G=(V,E)$, we say that node $v$ is \emph{reachable} from $u$ if there is a path from $u$ to $v$. Notice that, if $G$ is undirected, reachability is symmetric: if $u$ is reachable from $v$, then the reverse is true. If, for all $u,v\in V$, $u$ is reachable from $v$, then $G$ is connected. The largest subsets $V'\subseteq V$ such that the subgraph induced by $V'$ is connected are called the connected components of $G$.  

\bigskip
In link streams, we say that $u$ is reachable from $v$ if there is a path from $v$ to $u$. Notice that reachability is not symmetric: $u$ may be reachable from $v$ while $v$ is not reachable from $u$, see Figure~\ref{fig:reachability-stream}. If for all $u$ and $v$ in $V$ node $u$ is reachable from node $v$ then we say that $L$ is connected.

\begin{figure}
	\includegraphics{reachability-stream}
	\caption{Two streams $L$ and $L'$. In $L$, $v$ is reachable from $u$, but the reverse is not true: there is no path from $v$ to $u$. In $L'$, $u$ and $v$ are both reachable from each other: $v$ can reach $u$ through the link $(7,10,v,u)$.}
	\label{fig:reachability-stream}
\end{figure}

Given $X \subseteq V$ and $[b,e] \subseteq T$, we say that $C=(X,b,e)$ is a connected component if the substream $L_{b..e}(X)$ is connected. In other words, for any two nodes $u$ and $v$ in $X$ there is a path from $u$ to $v$ starting at $b$ or later, arriving at $e$ or earlier, and involving only nodes in $X$.

Notice that, if $C = (X,b,e)$ is a connected component then $C' = (X,b',e')$ also is a connected component if $b'\le b$ and $e'\ge e$.
As a consequence, if $(X,b,e)$ is a connected component then $(X,\alpha,\omega)$ also is a connected component, and so maximal connected components are characterized by their set of nodes only.
Conversely, we say that a connected component $C$ is a connecting component if $X$ is maximal and $[b,e]$ is minimal. See Figure~\ref{fig:connected-comps} for an illustration.

\begin{figure}
	\centering
	\includegraphics[width=0.6\linewidth]{connected-component}
	\caption{$(\{a,b,c\}, 0,8)$ is a connected component: from time $0$ to time $8$, the three nodes are all reachable from each other. This is not true for any time $e'<6$, as there is no path from $c$ to $a$. We say that if the set of nodes is maximal and the time interval is minimal, the connected component is a connecting component: this is the case for $(\{a,b,c\}, 6,6)$.}
	\label{fig:connected-comps}
\end{figure}

A connected part of $L$ is a triplet $(X,b,e)$ such that for any two nodes $u$ and $v$ in $X$ there is a path from $u$ to $v$ starting at $b$ or later and arriving at $e$ or earlier in $L$. Notice that a connected component necessarily is a connected part. On the contrary, the paths between nodes in $X$ may involve nodes outside $X$ and so connected parts are not necessarily connected components, see Figure~\ref{fig:connected-part}.

\begin{figure}
\centering
\includegraphics[width=.5\linewidth]{connected-part.eps}
\caption{$A=(\{a,b,c\}, 0,10)$ is a connected part: all nodes in $A$ are reachable from each other. However, $B=(\{a,b,c,d\}, 0,10)$ is not a connected part, since there is no path from $d$ to $b$. Yet, if one considers the substream $L(\{a,b,c\})$, $A$ is not a connected part. $d$ is a connecting node.}
\label{fig:connected-part}
\end{figure}

% \noteperso{parler des connecting nodes : ceux qui font qu'une connected part est connected ($d$ dans la figure); rapport a la centralite ? Aurore ?}

% generalize $k$-connectedness? vs duration of links?

% $\delta$-connectedness as a generalization of graphs?

% transitive closure? (reciprocal) reachability stream? reachability within a certain time?

\section{Centralities}

It is usual to measure the importance of nodes and links in graphs using metrics called centralities. As there may be various criteria for this importance, several notions of centralities coexist. The most widely used ones probably are closeness and betweenness centralities. The closeness of a node $v$ measures its proximity to other nodes:
$
\closeness(v) = \sum_{u\not=v} \frac{1}{d(v,u)}
$, where $d(v,u)$ is the distance from $v$ to $u$, \ie{} the length of the shortest path from $v$ to $u$. If there is no path from $u$ to $v$, then the two nodes are infinitely far, and $d(u,v)=\infty$.
The betweenness of a node $v$ measures the number of shortest paths in the graph that involve $v$:
$
\betweenness(v) = \sum_{x\neq y \neq v} \frac{\sigma(x,y,v)}{\sigma(x,y)}
$
where $\sigma(x,y,v)$ is the number of shortest paths from $x$ to $y$ involving $v$ and $\sigma(x,y)$ is the total number of shortest paths from $x$ to $y$. Notice that the betweenness centrality is only defined for connected graphs. See Figure~\ref{fig:centrality-graph} for an illustration.

\begin{marginfigure}
	\includegraphics[width=150px]{centrality-graph}
	\caption{In this toy graph; nodes $k$, $l$ and $m$ have similar values of closeness centrality: they are the closest to every node in the graph. $k$ has a high betweenness centrality: it is on all shortest paths between $\{a,b,c,d\}$ and $\{e,f,g,h,i,j\}$, whereas $l$ and $m$ have a low betweenness centrality.}
	\label{fig:centrality-graph}
\end{marginfigure}

\subsection{Closeness centrality}

In a link stream, we want the closeness of a node to capture how fast it may reach other nodes. We therefore define the closeness of node $v$ at time $t$, for all $\gamma > 0$, as:
$$
\closeness_t(v) = \sum_{u\not=v} \frac{1}{\timetoreach_t(v,u)}
$$
and the closeness of node $v$ in $L$ as its average closeness over all the duration of $L$:
$$
\closeness(v) = \frac{1}{\omega-\alpha}\int_{\alpha}^{\omega}\closeness_t(v) dt
$$
In other words, the closeness of $v$ is the average of the inverse of the time needed to reach a randomly chosen node from $v$ at a randomly chosen instant. See Figure~\ref{fig:closeness-stream}. If there is no path from $u$ to $v$ in the stream, then the two nodes are infinitely far, and $\timetoreach_t(u,v)=\infty$.

By convention, we consider that $\frac{1}{\infty}=0$. If there is a link at time $t$ between $u$ and $v$, then $\timetoreach_t(u,v)=\gamma$.

By extension, we define the closeness of $v$ in an interval $[b,e]\subset [\alpha, \omega]$:
$$
	\closeness_{b..e}(v) = \frac{1}{e-b}\int_{b}^{e}\closeness_t(v) dt
$$

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{stream-closeness}
	\caption{A link stream $L=([0,10], \{a,b,c\}, \{(2,7,a,b),(3,8,b,c)\})$. From time $t=1$, node $a$ reaches node $b$ at time $2$, and node $c$ at time $3$. The closeness centrality of node $a$ at time $1$ is then ${\cal C}_t(a) = \frac{1}{2} + \frac{1}{3}=\frac{1}{6}$. At time $t'=5$, there is a path from $a$ to $b$ and from $a$ to $c$, and ${\cal C}_{t'}(a) = 2$. At time $t''=9$, there is no path from $a$ to $b$ nor from $a$ to $c$, hence ${\cal C}_{t''}(a) = 0$, by convention.}
	\label{fig:closeness-stream}
\end{figure}

% WARNING: closeness undefined if $\gamma=0$: two linked nodes are infinitely close.

% We further define the closeness of a time instant $t$, which is the time to reach a randomly chosen node from another randomly chosen node at time $t$.

% \noteperso{$\closeness(t)$ = ... time to reach a randomly chosen node from another randomly chosen node at time $t$}

\subsection{Betweenness centrality}

We then define the betweenness of node $v$ in $L$ as:
\begin{equation}
\betweenness(v) = \sum_{x\neq y \neq v} \frac{\varphi(x,y,v)}{\varphi(x,y)}
\end{equation}
where $\varphi(x,y,v)$ is the number of shortest fastest paths from $x$ to $y$ involving $v$.

We define the betweenness of node $v$ from time $b$ to time $e$ as:
$
\betweenness_{b..e}(v) = \sum_{x\neq y \neq v} \frac{\varphi_{b..e}(x,y,v)}{\varphi(x,y)}
$
where $\varphi_{b..e}(x,y,v)$ is the number of shortest fastest paths from $x$ to $y$ involving $v$ starting at $b$ or before and arriving at $e$ or later. Notice that $\betweenness(v) = \betweenness_{\alpha..\omega}(v)$.

Then, the betweenness of node $v$ at time $t$ is
the fraction of shortest fastest paths between any two pairs of nodes involving $v$ at time $t$ :
$$
\betweenness_t(v) = \sum_{x\neq y \neq v} \frac{\varphi_t(x,y,v)}{\varphi(x,y)}
$$
where $\varphi_t(x,y,v)$ is the number of shortest fastest paths from $x$ to $y$ involving $v$ at time $t$ and $\varphi(x,y)$ is the total number of shortest fastest paths from $x$ to $y$ in $L$.

\begin{figure}
	\centering
	\includegraphics[width=0.6\linewidth]{counting-shortest-fastest}
	\caption{A link stream $L$. The number of shortest fastest paths from $a$ to $c$ from time $t$ is depicted in red: it is all the paths $((x,4,a,b), (4,4, b,c))$ for $x\in [1,4]$. We say that there are $3$ units of time paths.}
	\label{fig:counting-shortest-fastest}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.6\linewidth]{betweenness-stream}
	\caption{A link stream $L$. The number of shortest fastest paths from $a$ to $c$ involving $b$ is depicted in green, and represents $2$ units of time (from time $4$ to time $6$. The number of shortest fastest paths from $a$ to $c$ also contains the paths depicted in red, representing $2$ units of time (from time $1$ to time $3$). Hence, $\frac{\varphi_t(a,c,b)}{\varphi_t(a,c)}=\frac{1}{2}$.}
	\label{fig:betweenness-stream}
\end{figure}

When all links have duration $\omega-\alpha$, the betweenness in the link stream is equal to the betweenness in the induced graph.

Notice that $\betweenness(v)$ is {\em not} the average value of $\betweenness_t(v)$ over $[\alpha,\omega]$: we do not take into account the time during which $v$ is involved in considered paths. Indeed, we are interested in measuring the number of paths involving $v$; measuring this number weighted by the duration of the involvement of $v$ may be done by computing $\frac{1}{\omega-\alpha}\int_{\alpha}^{\omega}\betweenness_t(v) dt$.

The betweenness centrality we just defined evaluates the importance of nodes and time periods in function of very global objects: shortest fastest paths in the global stream, and the number of such paths in the whole stream. One may obtain a much more local view by studying betweenness in a substream $L_{b..e}$ of $L$. In between, one may study the fraction of all shortest fastest paths that begin and/or end between $b$ and $e$, which is different from studying the substream $L_{b..e}$, see Figure~\ref{fig:paths-b-e}.

\begin{marginfigure}[-6cm]
	\centering
	\includegraphics[width=150px]{paths-b-e}
	\caption{The fraction of all shortest fastest paths that begin and/or end between $b$ and $e$ is different from studying the substream $L_{2..6}$: indeed, $((5,6,a,b), (7,7,b,c))$ is a shortest fastest path from $a$ to $c$ starting before $b$, but is not contained in $L_{2..6}$.}
	\label{fig:paths-b-e}
\end{marginfigure}

\section{$k$-closure}

Given two nodes $u,v\in V$ and a time $t\in T$, we define the $k$-closure of $(u,v)$ at time $t$, as the difference between the maximum time $t'<t$ such that there exists a path $P = \left((t_i,u_i,v_i)\right)_{i=0..n}$ of length $n\leq k$ with $t_0 \geq t'$, $u_0=u$ and $v_n=v$ and $t_n\leq t$. We denote it by $\bot_k(u,v,t) = t-t'$~\sidenote{Notice that $\bot_k(u,v,t)\ne \bot_k(v,u,t)$.}. In other words, the $k$-closure of $(u,v)$ is the minimum time one has to go in the past before $t$ to find a path of length at most $k$ from $u$ to $v$ in the stream. See example in Figure~\ref{fig:kclosure}.

\begin{figure}
	\includegraphics{kclosure}
	\caption{Two streams $L$ and $L'$. In $L$, the $2$-closure of $(a,c)$ at time $t$ is at time $t'=3$: indeed, there is a path of length $2$ from $a$ to $c$ at $t'$. In $L'$, one finds $t'=8$, since there is a path of length $1$ from $a$ to $c$ at time $8$.}
	\label{fig:kclosure}
\end{figure}

% \section{$\Delta$-analysis}

% Pour les degres : juste dire que c'est pareil (y compris la relation avec densité) ?
% TODO : l'autre sens : on part d'un chemin dans $L$


\section{Conclusion}
% \noteperso{define trees, BFS, etc? cycles? arbre = set of all foremost?? random walks? greedy walks?}

% \noteperso{$\gamma$ may be a function of the links, involved nodes, time, and other complex features, thus capturing the fact that delay may depend on the link and vary with time...}

% \noteperso{Connectedness : hard to compute ! (cite Clemence)}

In this chapter, we define notions of paths in link streams. Unlike graphs, where one is typically interested in shortest paths, we describe three types of interesting paths between two nodes: the \emph{foremost} path, which is the first to arrive from a given time $t$, the \emph{fastest}, which is the one that has the smallest duration, and the \emph{shortest}, which minimizes the number of hops.   

We define notions of reachability and connectivity, as well as metrics of centrality to assess the importance of nodes in the stream.

As we saw in Chapter~\ref{cha:ls:context}, many works study path-related concepts in the literature. Studying how these notions can be unified with the ones presented in this chapter is a prospective work. 

A particularly interesting future work is to generalize more complex notions of graph theory based on paths: for example cycles, trees (and spanning trees), as well as random walks, greedy walks or traversals of the link stream.