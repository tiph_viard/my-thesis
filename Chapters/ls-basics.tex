\chapter{Basics}
\label{cha:ls:basics}

% \noteperso{QUESTIONS :\\
% normalisation ?\\
% un tableau récapitulatif ? un arbre ?\\
% comment on gere les bords (inter-contacts) ?\\
% correlations ?\\
% Define $\top(L)$ and $\bot(L)$ as the first time a link occurs and the last one? $\vdash$ $\dashv$ $<L>$ when time is reduced and nodes also?}

% expliquer la démarche, dvp

The goal of this chapter is to lay the foundations of our framework for describing sequences of interactions. In each section, we first recall the definition of the corresponding concept in graph theory, and then extend it to link streams.

\section{Link streams}

In a graph $G = (V,E)$, $V$ is the set of nodes and $E \subseteq V\times V$ is the set of links.

A link $(u,v)$ in $E$ means that the two nodes $u$ and $v$ are in relation. In the example of Figure~\ref{fig:basic-graph}, nodes $g$ and $e$ are in relation. Links are undirected: no distinction is made between $(u,v)$ and $(v,u)$.

We say that $G$ is simple if for all $(u,v)$ in $E$, $u\ne v$, and there are no multiple links between two nodes $u$ and $v$.

\begin{marginfigure}
	\begin{center}
	\includegraphics[width=100px]{basic-graph}
	\caption{A graph $G=(V,E)$, with $V=\{a,b,c,\dots,m\}$ and $E=\{(a,b), (b,e),(c,f)\dots\}$. $G$ has $n=14$ nodes and $m=22$ links.}
	\label{fig:basic-graph}
	\end{center}
\end{marginfigure}

\bigskip
In a link stream $L = (T,V,E)$, $T = [\alpha,\omega]$ is the time span of the stream, $V$ is the set of nodes, and $E\subseteq T \times T \times V \times V$ is the set of links.
We call $|V|$ the order of $L$ and we denote it by $n$; we call $|E|$ its size and we denote it by $|L| = m$; we call $|T|=\omega-\alpha$ its duration and denote it by $\overline{L}$. In the example of Figure~\ref{fig:ex-basics}, the link stream has order $n=4$, size $m=9$ and duration $|T|=20$ units of time.

A link $l = (b,e,u,v)$ in $E$ means that nodes $u$ and $v$ interacted from time $b$ to time $e$, and so $e\ge b$. We call $e-b$ the duration of $l$ and we denote it by $\overline{l}$. Links are undirected: we make no distinction between $(b,e,u,v)$ and $(b,e,v,u)$. In the example of Figure~\ref{fig:ex-basics}, nodes $b$ and $c$ interact from time $1$ to time $7$, nodes $a$ and $b$ interact from time $2$ to time $9$, and so on. 

We say that $L$ is simple if for all $l = (b,e,u,v)$ in $E$ we have $u \ne v$ and $e>b$, and 
for all $l=(b,e,u,v)$ and $l'=(b',e',u,v)$ we have $[b,e] \cap [b',e'] = \emptyset$. In the remainder of this thesis, all link streams are considered to be simple if not stated otherwise. 

For any $u$, $v$ in $V$ and $t$ in $T$, we say that $u$ and $v$ interact at time $t$ in $L$ if there is a link $(b,e,u,v)$ in $E$ with $t \in [b,e]$. We denote by $\tau(u,v) = \cup_{(b,e,u,v)\in E} [b,e]$ the set of times at which $u$ and $v$ interact.

\begin{figure}
\centering
\includegraphics[width=.95\textwidth]{ex-basics}
\caption{An example of link stream: $L = (T,V,E)$ with $T = [0,20]$, $V = \{a,b,c,d\}$, and $E = \{(1,7,b,c), (2,9,a,b), (5,10,a,c), \dots\}$. Each node is represented as a dotted horizontal line. Each link is represented by a line between the two horizontal lines representing involved nodes with dots at its extremities. The horizontal line attached to the link represents its duration.}
% \caption{
% A toy example of link stream: $L = (T,V,E)$ with $V = \{a,b,c,d,e\}$, $T = [0,20]$, and $E = \{(1,7,b,c), (2,9,a,b), (5,10,a,c), (8,11,b,c), (9,11,c,d), (13,18, a,c), (14,15,c,d), (15,20,a,b), (18,19,c,d)\}$. Each link is represented by a line with dots at their extremities, between the two horizontal lines representing involved nodes. The horizontal line between the nodes involved represents the duration of the link.
% }
\label{fig:ex-basics}
\end{figure}

\section{Sub-links and sub-streams}

Given two graphs $G = (V,E)$ and $G' = (V',E')$, $G'$ is a subgraph of $G$ if $V'\subseteq V$ and $E'\subseteq E$. This is denoted by $G'\subseteq G$. If $G'\subseteq G$ and $G\subseteq G'$ then $G=G'$.

Given a graph $G=(V,E)$ and a set of nodes $S\subseteq V$, the sub-graph induced by $S$ is $G(S) = (S, \{(u,v) \in E : u \in S, v\in S\})$. Given a set of links $S\subseteq E$, the sub-graph induced by $S$ is $G(S) = (\{u : (u,v)\in S\}, S)$. See Figure~\ref{fig:basic-subgraph} for an example.

\begin{marginfigure}
	\begin{center}
	\includegraphics[width=100px]{basic-subgraph}
	\caption{The two graphs in solid lines $G=(\{g,i,j,l,m\},\{(g,i), (l,j), \dots\})$ and $G'=(\{a,b,e\}, \{(a,b),(b,e)\})$ are two subgraphs of the graph of Figure~\ref{fig:basic-graph}. $G$ is the sub-graph induced by the set of nodes $\{g,i,j,l,m\}$ and $G'$ is the sub-graph induced by the set of links $\{(a,b),(b,e)\}$.}
	\label{fig:basic-subgraph}
	\end{center}
\end{marginfigure}

The \emph{null graph} $G_{\emptyset} = (\emptyset,\emptyset)$ is the graph with no nodes and no edges, and the edgeless graph of order $n$, $G_{\not E} = (V,\emptyset)$ with $|V|=n$ is the graph with $n$ nodes and no edges.

The intersection of two graphs $G$ and $G'$ is their largest common sub-graph, and is denoted by $G\cap G'$.

The union of two graphs $G=(V,E)$ and $G'=(V',E')$ is the graph $(V\cup V',E\cup E')$, and is denoted by $G\cup G'$.

% ex en marge ??

\bigskip
Given two link streams $L = (T,V,E)$ and $L' = (T',V',E')$, we say that $L'$ is a substream of $L$ if $V' \subseteq V$, $T' \subseteq T$, and for all $u$, $v$ in $V'$ and $t$ in $T'$, if $u$ and $v$ interact at time $t$ in $L'$ then they also interact at time $t$ in $L$~\sidenote{Notice however that $E'$ is \emph{not} included in $E$.}. We denote this by $L' \substreameq L$. If $L' \substreameq L$ and $L \substreameq L'$ then $L'=L$.

Given a link $l = (b,e,u,v)$, we say that $l' = (b',e',u',v')$ is a sub-link of $l$ if $u'=u$, $v'=v$, and $[b',e'] \subseteq [b,e]$; we denote this by $l' \sublinkeq l$. Notice that, if $L = (T,V,E)$ and $L' = (T',V',E')$ are simple link streams, then $L' \substreameq L$ if and only if for all $l'$ in $E'$ there is a $l$ in $E$ such that $l' \sublinkeq l$. In Figure~\ref{fig:ex-basics}, $L'=([0,10], \{a,b,c\}, \{(2,5,b,c), (2,7,a,b)\})$ is a sub-stream of $L$. See Figure~\ref{fig:basic-substream} for an example.

%Given a set of nodes $S \subseteq V$, we define the sub-stream $L(S)$ of $L$ induced by $S$ as $L(S) = (S,T,E(S))$, where $E(S)$ is the set of links involved in $S$: $E(S) = \{ (b,e,u,v) \in E,\ \exists u,v\in S\}$.

%Given a set of pairs of nodes $F \subseteq S \times S$, we define the sub-stream $L(F)$ of $L$ induced by $F$ as $L(F) = (S,T,E(F))$, where $E(F) = \{ (b,e,u,v) \in E,\ \exists (u,v)\in F\}$.

Given a link stream $L=(T,V,E)$ and a set of pairs of nodes $S \subseteq V \times V$, let us denote by $V(S)$ the set of nodes involved in $S$: $V(S) = \{ v\in V :  \exists (v,u)\in S\}$, and let us denote by $E(S)$ the set of links involved in $S$: $E(S) = \{ (b,e,u,v) \in E : (u,v)\in S\}$.
We define the sub-stream of $L$ induced by $S$ as $L(S) = (T,V(S),E(S))$.
By extension, if $S$ is a set of nodes then we define the sub-stream of $L$ induced by $S$ as $L(S) = L(S\times S)$.
We denote by $L(u,v)$ the sub-stream $L(\{(u,v)\})$ and by $L(v)$ the sub-stream $L(\{v\}\times V)$.
In the example of Figure~\ref{fig:ex-basics}, the sub-stream of $L$ induced by nodes $a,b$ is $L(\{a,b\}) = (T,\{a,b\},\{(2,9,a,b), (15,20,a,b)\})$, and the sub-stream induced by the pair of nodes $(c,d)$ is $L(\{(c,d)\}) = (T,\{c,d\},\{(9,11,c,d), (14,15,c,d), (18,19,c,d)\})$.

%In other words, $L(S)$ is the sub-stream $L' = (V',T,E')$ of $L$ such that $V'$ is minimal and $E'$ is maximal such that for all $l'=(b,e,u,v)$ in $E'$, $(u,v) \in S$.

Given a time interval $T' = [\alpha',\omega'] \subseteq T$, let us denote by $E_{T'} = E_{\alpha'..\omega'}$ the set of quadruplets $(b',e',u,v)$ such that there exists $(b,e,u,v)$ in $E$ with $[b',e'] = [b,e] \cap T'$. We define the sub-stream $L_{T'} = L_{\alpha'..\omega'}$ of $L$ induced by $T'$ as $(T',V,E_{T'})$. By extension, we denote by $L_t$ the sub-stream $L_{t..t}$ of duration $0$.

\begin{figure}
\centering
\includegraphics[width=.95\textwidth]{basic-substream}
\caption{The two streams in solid lines $L=([0,20],\{a,b\},\{(2,9,a,b),(15,20,a,b)\})$ and $L'=([0,20], \{c,d\}, \{(9,11,c,d),(14,15,c,d),\dots\})$ are two substreams of the stream of Figure~\ref{fig:ex-basics}. $L$ is the substream induced by the set of nodes $\{a,b\}$ and $L'$ is the substream induced by the pair of nodes $(c,d)$.}
\label{fig:basic-substream}
\end{figure}

%\noteperso{ci-dessus: garder les deux notations ? ou prendre $\alpha'$ et $\omega'$ dans $T$ ?}
The null link stream $L_{\emptyset}=(\emptyset,\emptyset,\emptyset)$ is the stream with no time, no nodes and no links. For any $T\subseteq\mathds{R}$, the order $0$ stream, \ie{ } the stream with no nodes and no edges, is $L_{\not V,\not E} = (T, \emptyset, \emptyset)$. For any $T\subseteq\mathds{R}$ and for any $n\in \mathds{N}$, the edgeless stream of order $n$ is the stream with $n$ nodes and no edges, \ie{ } $L_{\not E}=(T,V,\emptyset)$ with $|V|=n$.

We define the intersection of two link streams $L$ and $L'$ as their largest common sub-stream, and we denote it by $L \cap L'$.

We define the union of two link streams $L=(T,V,E)$ and $L'=(T',V',E')$ as the stream $(T'', V\cup V', E\cup E')$, where $T''$ is the smallest interval such that $T\subseteq T''$ and $T'\subseteq T''$. We denote it by $L\cup L'$. Notice that even if $L$ and $L'$ are simple, $L\cup L'$ is not necessarily simple.

Finally, we define $\sigma(L)$ the simplification of $L$ as the smallest sub-stream of $L$ (in terms of number of links) such that $\sigma(L)= L$ and for all $(b,e,u,v)$ in $\sigma(L)$, $e\geq b$. This necessarily is a simple link stream.

\section{Line stream}

The line graph $\hat{G}$ of $G=(V,E)$ is the graph $\hat{G} = (E,\hat{E})$ in which each node is a link of $G$ and two nodes are linked if they have an extremity in common: $((u,v),(x,y))$ is in $\hat{E}$ if $\{u,v\} \cap \{x,y\} \neq \emptyset$.

\begin{marginfigure}
	\centering
	\includegraphics[width=150px]{basic-line-graph}
	\caption{A graph $G=(V,E)$ and its line graph $\hat{G}$. In the line graph, there is a link between $(a,b)$ and $(b,d)$ because these two edges have node $b$ in common.}
	\label{fig:basic-line-graph}
\end{marginfigure}

\bigskip
The line stream $\hat{L}$ of $L=(T,V,E)$ is the link stream $\hat{L} = (T,\hat{V},\hat{E})$, with $\hat{V}=\{(u,v) : \exists (b,e,u,v)\in E\}$ and two nodes are linked if they share a node and are linked at the same time: $(b,e, (u,v), (x,y))$ is in $\hat{E}$ if $\{u,v\}\cap \{x,y\}\ne\emptyset$ and there exists two links $(b',e',u,v)$ and $(b'',e'',x,y)$ in $E$ such that $[b',e']\cap[b'',e'']=[b,e]$. See Figure~\ref{fig:line-stream} for an illustration. 
\begin{figure}
	\includegraphics[width=1\linewidth]{line-stream}
	\caption{A link stream $L=([0,10], \{a,b,c\}, \{(1,8,a,b), (3,4,b,c), (5,9, a,c)\}$ and its corresponding line stream $\hat{L} = ([0,10], \{(a,b), (b,c), (a,c)\}, \hat{E})$, with $\hat{E}=\{(\left(3,4,(a,b),(b,c)\right),(\left(5,8,(a,b),(a,c)\right)\}$. There is no link between $(b,c)$ and $(a,c)$ because these two pairs of nodes do not have links at the same time in $L$.}
	\label{fig:line-stream}
\end{figure} 

Just like for graphs, the line stream of the line stream, $\hat{\hat{L}}$, is not equal to $L$.

\section{Induced graphs}

Given a link stream $L = (T,V,E)$, we define its induced graph $G(L) = (V,E')$ where $(u,v) \in E'$ if and only if there exist $b$ and $e$ such that $(b,e,u,v) \in E$. In other words, it is the graph in which there is a link between two nodes if they interacted at least once in $L$. One may in addition associate to each link $(u,v)$ in $E'$ a weight $w(u,v)= |L(u,v)|$ capturing the number of interactions, $w(u,v) = \sum_{l\in L(u,v)} \overline{l}$ capturing their total duration, or other quantities of interest.

\begin{figure}
	\centering
	% Link stream example and its induced graph ? Or in margin ?
	\includegraphics[width=0.6\linewidth]{induced-graph}
	\caption{The graph $G = (\{a,b,c,d\}, \{(a,b),(a,c),(b,c),(c,d)\})$ induced by the link stream $L$ of Figure~\ref{fig:ex-basics} (left), and the graph $G_{12..20}(\{a,b,c\}) = (\{a,b,c\}, \{(a,b), (a,c)\})$ induced by the sub-stream of $L$ induced by nodes $a,b$ and $c$ over interval $[12,20]$, $L_{12..20}(\{a,b,c\})$.}
	\label{fig:induced-graphs}
\end{figure}

Given $L$, we denote by $G_{t..t'}$ the graph $G(L_{t..t'})$, by $G_t$ the graph $G(L_t) = G_{t..t}$, and by $G(S)$ the graph $G(L(S))$.
Figure~\ref{fig:induced-graphs} displays $G(L)$ for the example of Figure~\ref{fig:ex-basics}, and for the graph induced by nodes $a$, $b$ and $c$ from time $12$ to time $20$, $G_{12..20}(\{a,b,c\})$.

% \section{Basic statistics}

% \noteperso{Section utile ? Qu'y définir ?}

% Let us consider a link $l=(b,e,u,v)$ in $L$. We call $e-b$ the duration of $l$ and denote it by $\overline{l}$.  
% Let $e'$ be the largest value in $T$ such that $e'<b$ and there is a link $l'=(b',e',u,v)$ in $L$. If such a value exists, we define the inter-contact time of $l$ as $\tau(l) = b-e'$. Que fait-on aux bords ??

% For any two nodes $u$ and $v$, the number of occurrences of $(u,v)$ is the number of links involving them: $o(u,v) = ||L(u,v)||$. We define the frequency $f(u,v)$ as the probability that there is a link between them at a randomly chosen instant $t$: $f(u,v) = \frac{\sum_{l\in L(u,v)} \overline{l}}{\omega-\alpha}$.

% We define similarly the duration, inter-contact times, and numbers of occurrences of nodes.  

% Example

% The dynamics of interactions between two nodes $u$ and $v$ is often described by their number of occurrences $o(u,v)$, their frequency $f(u,v)$, as well as the duration and inter-contact time distributions of links involving them\,~\sidenote{ The duration distribution of $L$ is the fraction of links in $L$ with duration $k$, for all $k$: $ D_k = \frac{|\{l \in L,\ \overline{l} = k\}|}{||L||} $ } \cite{}.
% More globally, the distribution of the numbers of occurrences of pairs of nodes in $L$ and the distribution of their frequencies are key descriptors of $L$, as well as the distribution of all link durations and the one their inter-contact times \cite{}. We display them for our two real-world examples in Figure~\ref{x}. One may also study correlations between these values (indicating for instance if links with long duration tend to have a long inter-contact time or not).

% Complementary to notions above, let us say that a link $(b,e,u,v)$ in $L$ is active at time $t$ if $t\in [b,e]$. Likewise, we say that $u$ and $v$ are active at these times. We then define $n_t(L)$ as the number of nodes active at time $t$, and $m_t(L)$ as the number of links active at time $t$. We plot these values for our two datasets in figure x. We plot the distrubutions of these values in figure x (average of this distribution = expected value for random $t$).  

% Going further, one may observe the distribution of durations of links active at a given time $t$, their inter-contact times, etc.

% Data. acceleration = derivee de $n_t$ ?

% la frequence est une sorte de vitesse ? et l'acceleration ?

\section{Conclusion}
% \noteperso{Parler de $\Delta$ variables? de liens pondérés, dirigés ?}
We presented in this first chapter the basis of our formalism. We defined link streams and substreams, line streams, and graphs induced by streams. We focused on simple, undirected and unweighted streams.

Adapting these definitions to directed link streams is just a matter of making a difference between $(b,e,u,v)$ and $(b,e,v,u)$, and defining notions of substreams, line streams and so on subsequently. Similarly, loops can be taken into account by authorizing links $(b,e,u,v)$ in $E$ such that $u=v$.

Using these definitions, one could easily define a weighted link stream as $L = (T,V,E)$, where $E\subseteq T\times T\times V\times V\times \mathds{R}$. Elements of $E$ would then be $(b,e,u,v,w)$, where $w$ is the weight associated to the link.