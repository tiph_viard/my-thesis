" ATP project vim script: mar. mai 17, 2016 at 03:52  +0200.

let b:atp_MainFile = 'thesis.tex'
let g:atp_mapNn = 0
let b:atp_autex = 1
let b:atp_TexCompiler = 'pdflatex'
let b:atp_TexOptions = '-synctex=1'
let b:atp_TexFlavor = 'tex'
let b:atp_auruns = '1'
let b:atp_ReloadOnError = '1'
let b:atp_OutDir = '/home/viard/Documents/my-thesis'
let b:atp_OpenViewer = '1'
let b:atp_XpdfServer = 'thesis'
let b:atp_Viewer = 'okular'
let b:TreeOfFiles = {'Chapters/chapter1.tex': [{}, 318], 'Chapters/chapter2.tex': [{}, 319], 'Chapters/chapter3.tex': [{'Chapters/preuve_inc.tex': [{}, 155], 'Chapters/algorithm.tex': [{}, 81]}, 320], 'Chapters/chapter4.tex': [{}, 321], 'Chapters/chapter5.tex': [{}, 322]}
let b:ListOfFiles = ['Chapters/chapter1.tex', 'Chapters/chapter2.tex', 'Chapters/chapter3.tex', 'Chapters/chapter4.tex', 'Chapters/chapter5.tex', 'thesis.bib', 'Chapters/algorithm.tex', 'Chapters/preuve_inc.tex']
let b:TypeDict = {'Chapters/chapter1.tex': 'input', 'Chapters/chapter2.tex': 'input', 'Chapters/chapter3.tex': 'input', 'Chapters/chapter4.tex': 'input', 'Chapters/chapter5.tex': 'input', 'Chapters/algorithm.tex': 'input', 'Chapters/preuve_inc.tex': 'input', 'thesis.bib': 'bib'}
let b:LevelDict = {'Chapters/chapter1.tex': 1, 'Chapters/chapter2.tex': 1, 'Chapters/chapter3.tex': 1, 'Chapters/chapter4.tex': 1, 'Chapters/chapter5.tex': 1, 'Chapters/algorithm.tex': 2, 'Chapters/preuve_inc.tex': 2, 'thesis.bib': 1}
let b:atp_BibCompiler = 'bibtex'
let b:atp_StarEnvDefault = ''
let b:atp_StarMathEnvDefault = ''
let b:atp_updatetime_insert = 4000
let b:atp_updatetime_normal = 2000
let b:atp_LocalCommands = ['\ALG@printindent', '\noteperso{', '\dclique', '\clique{', '\ie', '\sublinkeq', '\substreameq', '\distance', '\latency', '\timetoreach', '\closeness', '\betweenness', '\Cuv', '\Cu', '\Cv', '\uinterv', '\nC', '\mC', '\dC', '\compC', '\linegraph', '\hangp{', '\hangstar', '\vdqi', '\ei', '\ve', '\be', '\VDQI', '\EI', '\VE', '\BE', '\TL', '\monthyear', '\openepigraph{', '\blankpage', '\measure{', '\hlred{', '\hangleft{', '\hairsp', '\hquad', '\TODO', '\eg', '\na', '\tXeLaTeX', '\tuftebs', '\doccmdnoindex', '\doccmddef', '\doccmd', '\docopt{', '\docarg{', '\docenv{', '\docenvdef{', '\docpkg{', '\doccls{', '\docclsopt{', '\docclsoptdef{', '\docmsg{', '\docfilehook{', '\doccounter{', '\ff']
let b:atp_LocalEnvironments = ['lemma', 'theorem', 'docspec']
let b:atp_LocalColors = []
