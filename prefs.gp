# prefs.gp

set grid

set xlabel font ",24"
set ylabel font ",24"

set xtics font ",20"
set ytics font ",20"

set key font ",22"