\contentsline {chapter}{Introduction}{11}{chapter*.1}
\contentsline {part}{I\hspace {1em}A basic language for link streams}{15}{part.1}
\contentsline {xpart}{A basic language for link streams}{17}{part.1}
\contentsline {chapter}{\numberline {1}Context}{19}{chapter.1}
\contentsline {section}{\numberline {1.1}Models inducing information loss}{19}{section.1.1}
\contentsline {section}{\numberline {1.2}Models inducing no information loss}{22}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Static graphs from sequences of interactions}{22}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Temporal networks}{23}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Time-Varying Graphs}{24}{subsection.1.2.3}
\contentsline {section}{\numberline {1.3}Conclusion}{25}{section.1.3}
\contentsline {chapter}{\numberline {2}Basics}{27}{chapter.2}
\contentsline {section}{\numberline {2.1}Link streams}{27}{section.2.1}
\contentsline {section}{\numberline {2.2}Sub-links and sub-streams}{28}{section.2.2}
\contentsline {section}{\numberline {2.3}Line stream}{30}{section.2.3}
\contentsline {section}{\numberline {2.4}Induced graphs}{31}{section.2.4}
\contentsline {section}{\numberline {2.5}Conclusion}{31}{section.2.5}
\contentsline {chapter}{\numberline {3}Density-based notions}{33}{chapter.3}
\contentsline {section}{\numberline {3.1}Density, neighborhood and degree}{33}{section.3.1}
\contentsline {section}{\numberline {3.2}Node clusters}{36}{section.3.2}
\contentsline {section}{\numberline {3.3}Cliques}{42}{section.3.3}
\contentsline {section}{\numberline {3.4}Clustering coefficient and transitivity ratio}{43}{section.3.4}
\contentsline {section}{\numberline {3.5}Conclusion}{45}{section.3.5}
\contentsline {chapter}{\numberline {4}Paths-based notions}{47}{chapter.4}
\contentsline {section}{\numberline {4.1}Paths}{47}{section.4.1}
\contentsline {section}{\numberline {4.2}Connectivity}{49}{section.4.2}
\contentsline {section}{\numberline {4.3}Centralities}{50}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Closeness centrality}{51}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Betweenness centrality}{52}{subsection.4.3.2}
\contentsline {section}{\numberline {4.4}$k$-closure}{54}{section.4.4}
\contentsline {section}{\numberline {4.5}Conclusion}{54}{section.4.5}
\contentsline {chapter}{\numberline {5}$\Delta $-analysis of link streams}{57}{chapter.5}
\contentsline {section}{\numberline {5.1}$\Delta $-analysis and instantaneous links}{57}{section.5.1}
\contentsline {section}{\numberline {5.2}Density}{58}{section.5.2}
\contentsline {section}{\numberline {5.3}Degree}{59}{section.5.3}
\contentsline {section}{\numberline {5.4}Clusters}{59}{section.5.4}
\contentsline {section}{\numberline {5.5}Conclusion}{59}{section.5.5}
\contentsline {chapter}{\numberline {6}Computing cliques in link streams}{61}{chapter.6}
\contentsline {section}{\numberline {6.1}Definitions}{61}{section.6.1}
\contentsline {section}{\numberline {6.2}Algorithm}{62}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Description of the algorithm}{62}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Proof of correctness}{65}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Computational complexity}{69}{subsection.6.2.3}
\contentsline {section}{\numberline {6.3}Experiments}{71}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Dataset}{71}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}Impact of data structure}{72}{subsection.6.3.2}
\contentsline {subsection}{\numberline {6.3.3}Examining $\Delta $-cliques}{73}{subsection.6.3.3}
\contentsline {section}{\numberline {6.4}Conclusion}{76}{section.6.4}
\contentsline {chapter}{\numberline {7}Conclusion and perspectives}{79}{chapter.7}
\contentsline {part}{II\hspace {1em}Application to IP traffic}{81}{part.2}
\contentsline {xpart}{Application to IP traffic}{83}{part.2}
\contentsline {chapter}{\numberline {8}Context}{85}{chapter.8}
\contentsline {section}{\numberline {8.1}Sources of data}{85}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}CAIDA}{85}{subsection.8.1.1}
\contentsline {subsection}{\numberline {8.1.2}MAWI}{86}{subsection.8.1.2}
\contentsline {subsection}{\numberline {8.1.3}USC ANT}{86}{subsection.8.1.3}
\contentsline {subsection}{\numberline {8.1.4}KDD Cup}{87}{subsection.8.1.4}
\contentsline {section}{\numberline {8.2}Analysis of IP traffic}{87}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}Graph-based}{87}{subsection.8.2.1}
\contentsline {subsection}{\numberline {8.2.2}Time-series analysis}{88}{subsection.8.2.2}
\contentsline {subsection}{\numberline {8.2.3}Machine-learning}{89}{subsection.8.2.3}
\contentsline {section}{\numberline {8.3}The MAWI dataset}{92}{section.8.3}
\contentsline {subsection}{\numberline {8.3.1}Traffic captures}{92}{subsection.8.3.1}
\contentsline {subsection}{\numberline {8.3.2}MAWILab event database}{95}{subsection.8.3.2}
\contentsline {chapter}{\numberline {9}MAWI traffic as a link stream}{97}{chapter.9}
\contentsline {section}{\numberline {9.1}Traffic modelling}{97}{section.9.1}
\contentsline {section}{\numberline {9.2}Bipartite link streams}{99}{section.9.2}
\contentsline {subsection}{\numberline {9.2.1}Density and clustering coefficient}{101}{subsection.9.2.1}
\contentsline {subsection}{\numberline {9.2.2}Projection}{101}{subsection.9.2.2}
\contentsline {subsection}{\numberline {9.2.3}Redundancy}{102}{subsection.9.2.3}
\contentsline {section}{\numberline {9.3}Conclusion}{103}{section.9.3}
\contentsline {chapter}{\numberline {10}Analysis of IP traffic}{105}{chapter.10}
\contentsline {section}{\numberline {10.1}Our approach}{105}{section.10.1}
\contentsline {subsection}{\numberline {10.1.1}Event characterization}{106}{subsection.10.1.1}
\contentsline {subsection}{\numberline {10.1.2}Event identification through manual inspection}{106}{subsection.10.1.2}
\contentsline {subsection}{\numberline {10.1.3}Hierarchy of features for the analysis of IP traffic}{107}{subsection.10.1.3}
\contentsline {subsection}{\numberline {10.1.4}Analysis of IP traffic process}{109}{subsection.10.1.4}
\contentsline {section}{\numberline {10.2}Results}{111}{section.10.2}
\contentsline {subsection}{\numberline {10.2.1}Preliminary features}{111}{subsection.10.2.1}
\contentsline {subsection}{\numberline {10.2.2}Basic features on the link stream}{119}{subsection.10.2.2}
\contentsline {subsection}{\numberline {10.2.3}Comparison with MAWILab}{120}{subsection.10.2.3}
\contentsline {section}{\numberline {10.3}Conclusion}{121}{section.10.3}
\contentsline {chapter}{\numberline {11}Conclusions and perspectives}{127}{chapter.11}
\contentsline {part}{III\hspace {1em}Conclusions and perspectives}{131}{part.3}
\contentsline {xpart}{Conclusions and perspectives}{133}{part.3}
\contentsline {chapter}{\numberline {12}Summary and Contributions}{133}{chapter.12}
\contentsline {chapter}{\numberline {13}Perspectives}{137}{chapter.13}
\contentsline {section}{\numberline {13.1}Generalizing link streams}{137}{section.13.1}
\contentsline {subsection}{\numberline {13.1.1}Computational considerations}{138}{subsection.13.1.1}
\contentsline {subsection}{\numberline {13.1.2}Algorithmic considerations}{139}{subsection.13.1.2}
\contentsline {section}{\numberline {13.2}Modules and dense groups}{140}{section.13.2}
\contentsline {subsection}{\numberline {13.2.1}Modular decomposition}{140}{subsection.13.2.1}
\contentsline {subsection}{\numberline {13.2.2}Dense groups and communities}{142}{subsection.13.2.2}
\contentsline {subsection}{\numberline {13.2.3}Generation of synthetic link streams}{144}{subsection.13.2.3}
\contentsline {subsection}{\numberline {13.2.4}Visualization}{146}{subsection.13.2.4}
\contentsline {subsection}{\numberline {13.2.5}Event detection in link streams}{149}{subsection.13.2.5}
\contentsline {subsection}{\numberline {13.2.6}Applications}{150}{subsection.13.2.6}
\contentsline {section}{\numberline {13.3}Prediction}{151}{section.13.3}
\contentsline {chapter}{Appendices}{153}{section*.2}
\contentsline {chapter}{\numberline {A}Analysis of the temporal and structural features of threads in a mailing-list}{155}{Appendix.1.A}
\contentsline {section}{\numberline {A.1}Introduction}{155}{section.1.A.1}
\contentsline {section}{\numberline {A.2}Dataset}{156}{section.1.A.2}
\contentsline {section}{\numberline {A.3}Framework and notations}{158}{section.1.A.3}
\contentsline {section}{\numberline {A.4}Basic statistics}{159}{section.1.A.4}
\contentsline {section}{\numberline {A.5}Interactions within threads}{161}{section.1.A.5}
\contentsline {subsection}{\numberline {A.5.1}Density of threads}{162}{subsection.1.A.5.1}
\contentsline {subsection}{\numberline {A.5.2}Intra-thread density}{163}{subsection.1.A.5.2}
\contentsline {section}{\numberline {A.6}Relations between threads}{164}{section.1.A.6}
\contentsline {subsection}{\numberline {A.6.1}Inter-thread density}{164}{subsection.1.A.6.1}
\contentsline {subsection}{\numberline {A.6.2}Graphs between threads}{166}{subsection.1.A.6.2}
\contentsline {subsection}{\numberline {A.6.3}Quotient stream}{168}{subsection.1.A.6.3}
\contentsline {section}{\numberline {A.7}Conclusion}{168}{section.1.A.7}
\contentsline {chapter}{\numberline {B}Identifying Roles in an IP Network with Temporal and Structural Density}{171}{Appendix.1.B}
\contentsline {section}{\numberline {B.1}Introduction}{171}{section.1.B.1}
\contentsline {section}{\numberline {B.2}Notion of $\Delta $-density}{173}{section.1.B.2}
\contentsline {subsection}{\numberline {B.2.1}Framework}{173}{subsection.1.B.2.1}
\contentsline {subsection}{\numberline {B.2.2}$\Delta $-density of links}{174}{subsection.1.B.2.2}
\contentsline {subsection}{\numberline {B.2.3}$\Delta $-density of streams and sets of nodes}{176}{subsection.1.B.2.3}
\contentsline {section}{\numberline {B.3}Identifying roles}{176}{section.1.B.3}
\contentsline {subsection}{\numberline {B.3.1}Our datasets}{177}{subsection.1.B.3.1}
\contentsline {subsection}{\numberline {B.3.2}Identifying relevant $\Delta $}{177}{subsection.1.B.3.2}
\contentsline {subsection}{\numberline {B.3.3}Neighborhoods and clustering coefficient}{181}{subsection.1.B.3.3}
\contentsline {subsection}{\numberline {B.3.4}Interpretation}{184}{subsection.1.B.3.4}
\contentsline {section}{\numberline {B.4}Conclusion}{185}{section.1.B.4}
\contentsfinish 
